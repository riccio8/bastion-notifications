# Bastion Notifications

[![NuGet](https://img.shields.io/nuget/v/Bastion.Notifications.svg?maxAge=3600)](https://www.nuget.org/packages/Bastion.Notifications/)
[![NuGet](https://img.shields.io/nuget/v/Bastion.Notifications.EntityFrameworkCore.svg?maxAge=3600)](https://www.nuget.org/packages/Bastion.Notifications.EntityFrameworkCore/)
