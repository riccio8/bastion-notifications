﻿using Bastion.Notifications.Constants;
using Bastion.Notifications.EntityFrameworkCore;
using Bastion.Notifications.Hubs;

using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.OpenApi.Models;

using Swashbuckle.AspNetCore.Filters;

using System;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.Json.Serialization;

namespace BastionNotificationsSample.Api
{
    /// <summary>
    /// Startup.
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="Startup"/> class.
        /// </summary>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration.
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// ConfigureServices.
        /// </summary>
        /// <param name="services"></param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContextPool<NotificationDbContext>(options =>
                //options.UseSqlServer(Configuration.GetConnectionString("msSqlConnection"),
                options.UseNpgsql(Configuration.GetConnectionString("pgSqlConnection"),
                b => b.MigrationsAssembly("BastionNotificationsSample.Api")));

            services.AddNotifications()
                .AddConfiguration(config => config.IdLength = 12)
                .AddEntityFrameworkRepositories<NotificationDbContext>();

            services.AddAuthorization(options =>
            {
                options.AddPolicy(NotificationPolicyConstants.ManagementPolicy,
                    policy => policy.RequireRole("Operator"));
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                .AddJwtBearer(options =>
                {
                    options.RequireHttpsMetadata = false;
                });

            services.AddSignalR();

            services.AddControllers()
                .AddJsonOptions(options =>
                {
                    options.JsonSerializerOptions.IgnoreNullValues = true;
                    options.JsonSerializerOptions.PropertyNameCaseInsensitive = true;
                    options.JsonSerializerOptions.Converters.Add(new JsonStringEnumConverter());
                });

            services.AddSwaggerGen(options =>
            {
                options.SwaggerDoc("v1", new OpenApiInfo
                {
                    Title = "API",
                    Version = "v1",
                    Description = $"Version: <b>{Assembly.GetEntryAssembly().GetName().Version.ToString()}</b>"
                });

                Directory.GetFiles(AppContext.BaseDirectory, "*.xml").ToList().ForEach(file => options.IncludeXmlComments(file, true));

                options.OperationFilter<AppendAuthorizeToSummaryOperationFilter>();
                options.AddSecurityDefinition("oauth2", new OpenApiSecurityScheme
                {
                    Name = "Authorization",
                    In = ParameterLocation.Header,
                    Type = SecuritySchemeType.ApiKey,
                    Description = "JWT Authorization header using the Bearer scheme. Example: \"Bearer {token}\""
                });
                options.OperationFilter<SecurityRequirementsOperationFilter>(false);
                options.DescribeAllParametersInCamelCase();
                options.GeneratePolymorphicSchemas();
                options.EnableAnnotations();
            });
        }

        /// <summary>
        /// Configure.
        /// </summary>
        /// <param name="app">Application builder.</param>
        /// <param name="env">WebHost environment.</param>
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapHub<NotificationsHub>("/hubNotifications");
            });

            app.UseSwagger();
            app.UseSwaggerUI(configure =>
            {
                configure.RoutePrefix = string.Empty;
                configure.SwaggerEndpoint("/swagger/v1/swagger.json", "My API V1");
            });
        }
    }
}