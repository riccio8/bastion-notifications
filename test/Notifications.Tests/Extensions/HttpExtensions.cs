﻿using System.Diagnostics.CodeAnalysis;
using System.Net.Http;
using System.Net.Mime;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace Notifications.Tests.Extensions
{
    [ExcludeFromCodeCoverage]
    internal static class HttpExtensions
    {
        private static readonly JsonSerializerOptions options = new JsonSerializerOptions() { PropertyNameCaseInsensitive = true, IgnoreNullValues = true };

        public static async Task<T> ReadAsAsync<T>(this HttpResponseMessage response)
        {
            using var responseStream = await response.Content.ReadAsStreamAsync();
            return await JsonSerializer.DeserializeAsync<T>(responseStream, options);
        }

        public static StringContent ToStringContent<T>(this T model)
        {
            return new StringContent(JsonSerializer.Serialize(model, options), Encoding.UTF8, MediaTypeNames.Application.Json);
        }
    }
}