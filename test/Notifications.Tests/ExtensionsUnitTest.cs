using Bastion.Notifications.Api.Models.Extensions;
using Bastion.Notifications.Api.Models.Requests;
using Bastion.Notifications.Extensions;

using System.Diagnostics.CodeAnalysis;

using Xunit;

namespace Notifications.Tests
{
    [ExcludeFromCodeCoverage]
    public class ExtensionsUnitTest
    {
        [Theory(DisplayName = "Generate identifier")]
        [InlineData(1)]
        [InlineData(8)]
        [InlineData(20)]
        [InlineData(30)]
        public void GenerateIdentifierTest(int length)
        {
            var identifier = IdentifierExtensions.GenerateIdentifier(length);

            Assert.Equal(identifier.Length, length);
        }

        [Theory(DisplayName = "Filter is read")]
        [MemberData(nameof(NotificationsData.FilterRequests), MemberType = typeof(NotificationsData))]
        public void ConvertIntToBoolean(NotificationsFilterRequest request, bool? expected)
        {
            var isRead = NotificationRequestExtensions.ToIsRead(request);

            Assert.Equal(expected, isRead);
        }

        [Theory(DisplayName = "Contains in body")]
        [InlineData("{\"description\":\"Not good\"}", "good")]
        [InlineData("{\"description\":\"Not good\"}", "Not good")]
        [InlineData("{\"description\":\"Not good\"}", "description")]
        public void ContainsInBody(string body, string search)
        {
            var condition = body.ToUpperInvariant().Contains(search.ToUpperInvariant());
            Assert.True(condition);
        }
    }
}