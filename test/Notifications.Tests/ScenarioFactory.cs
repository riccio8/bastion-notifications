﻿using Bastion.Notifications.EntityFrameworkCore;

using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc.Testing;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

using System.Diagnostics.CodeAnalysis;
using System.Linq;

namespace Notifications.Tests
{
    [ExcludeFromCodeCoverage]
    public class ScenarioFactory<TEntryPoint> : WebApplicationFactory<TEntryPoint> where TEntryPoint : class
    {
        protected override void ConfigureWebHost(IWebHostBuilder builder)
        {
            builder.ConfigureServices(services =>
            {
                var descriptor = services.SingleOrDefault(service
                    => service.ServiceType == typeof(DbContextOptions<NotificationDbContext>));
                if (descriptor != null)
                {
                    services.Remove(descriptor);
                }

                services.AddDbContextPool<NotificationDbContext>(options
                    => options.UseInMemoryDatabase("InMemoryDbForTesting"));

                using var scope = services.BuildServiceProvider().CreateScope();
                scope.ServiceProvider.GetRequiredService<NotificationDbContext>().Database.EnsureCreated();
            });
        }
    }
}