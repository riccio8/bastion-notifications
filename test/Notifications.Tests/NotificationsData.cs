﻿using Bastion.Notifications.Api.Models.Requests;

using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;

namespace Notifications.Tests
{
    [ExcludeFromCodeCoverage]
    internal class NotificationsData
    {
        public static IEnumerable<object[]> FilterRequests =>
            new List<object[]>
            {
                new object[] { new NotificationsFilterRequest { Read = 1, Unread = 1 }, null },
                new object[] { new NotificationsFilterRequest { Read = 0, Unread = 0 }, null },
                new object[] { new NotificationsFilterRequest { Read = 1, Unread = null }, true },
                new object[] { new NotificationsFilterRequest { Read = null, Unread = 1 }, false },
                new object[] { new NotificationsFilterRequest { Read = null, Unread = null }, null },
            };
    }
}