﻿namespace Bastion.Notifications.Api.Models
{
    /// <summary>
    /// Notification direction.
    /// </summary>
    public enum NotificationDirection
    {
        /// <summary>
        /// Both.
        /// </summary>
        Both = 0,

        /// <summary>
        /// Incoming.
        /// </summary>
        Incoming = 1,

        /// <summary>
        /// Outgoing.
        /// </summary>
        Outgoing = -1,
    }
}