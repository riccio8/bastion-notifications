﻿using System.Collections.Generic;

namespace Bastion.Notifications.Api.Models.Responses
{
    /// <summary>
    /// Notification detail response.
    /// </summary>
    public class NotificationDetailResponse : NotificationShortResponse
    {
        /// <summary>
        /// Body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// From user id.
        /// </summary>
        public string FromUserId { get; set; }

        /// <summary>
        /// To user id.
        /// </summary>
        public string ToUserId { get; set; }

        /// <summary>
        /// Previous id.
        /// </summary>
        public string PreviousId { get; set; }

        /// <summary>
        /// Answer notification id.
        /// </summary>
        public string AnswerNotificationId { get; set; }

        /// <summary>
        /// Array of id attachments.
        /// </summary>
        public ICollection<string> Attachments { get; set; }
    }
}