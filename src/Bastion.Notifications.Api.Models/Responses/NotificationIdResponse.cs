﻿namespace Bastion.Notifications.Api.Models.Responses
{
    /// <summary>
    /// Notification id response.
    /// </summary>
    public class NotificationIdResponse
    {
        /// <summary>
        /// Id.
        /// </summary>
        public string Id { get; set; }
    }
}