﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Constants;

using System;

namespace Bastion.Notifications.Api.Models.Responses
{
    /// <summary>
    /// Notification short response.
    /// </summary>
    public class NotificationShortResponse : NotificationIdResponse, INotifiable
    {
        /// <summary>
        /// Type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Sent ts.
        /// </summary>
        public DateTime SentTs { get; set; }

        /// <summary>
        /// Answer ts.
        /// </summary>
        public DateTime? AnswerTs { get; set; }

        /// <summary>
        /// Answer id.
        /// </summary>
        public char AnswerId { get; set; }

        /// <summary>
        /// Previous answer id.
        /// </summary>
        public char? PreviousAnswerId { get; set; }

        /// <summary>
        /// From or To user name.
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// To user name.
        /// </summary>
        public string ToUserName { get; set; }

        /// <summary>
        /// From userName.
        /// </summary>
        public string FromUserName { get; set; }

        /// <summary>
        /// Is read.
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// Direction.
        /// </summary>
        public NotificationDirection Direction { get; set; }

        /// <summary>
        /// Status.
        /// </summary>
        public string Status => GetStatus();

        /// <summary>
        /// SignalR method.
        /// </summary>
        public string NotificationMethod => "onNotification";

        private string GetStatus()
        {
            return (AnswerId) switch
            {
                NotificationAnswerConstants.Reject => "Rejected",
                NotificationAnswerConstants.Pending => "Pending",
                NotificationAnswerConstants.Approve => "Approved",
                NotificationAnswerConstants.Completed => "Completed",
                _ => default,
            };
        }
    }
}