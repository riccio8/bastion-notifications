﻿namespace Bastion.Notifications.Api.Models.Responses
{
    /// <summary>
    /// Notification rule user response.
    /// </summary>
    public class NotificationRuleUserResponse
    {
        /// <summary>
        /// User id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Rule id.
        /// </summary>
        public string RuleId { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        public string UserName { get; set; }
    }
}