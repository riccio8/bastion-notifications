﻿using Bastion.Notifications.Api.Models.Base;

using System.Collections.Generic;

namespace Bastion.Notifications.Api.Models.Responses
{
    /// <summary>
    /// Notification rule response.
    /// </summary>
    public class NotificationRuleResponse : NotificationRuleBase
    {
        /// <summary>
        /// Rule users.
        /// </summary>
        public IEnumerable<NotificationRuleUserResponse> RuleUsers { get; set; }
    }
}