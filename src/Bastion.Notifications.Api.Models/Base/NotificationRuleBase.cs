﻿namespace Bastion.Notifications.Api.Models.Base
{
    /// <summary>
    /// Notification rule base.
    /// </summary>
    public class NotificationRuleBase
    {
        /// <summary>
        /// Name.
        /// </summary>
        public virtual string Name { get; set; }

        /// <summary>
        /// Rule id.
        /// </summary>
        public virtual string RuleId { get; set; }

        /// <summary>
        /// Template id pending.
        /// </summary>
        public string TemplateIdPending { get; set; }

        /// <summary>
        /// Template id approved.
        /// </summary>
        public string TemplateIdApproved { get; set; }

        /// <summary>
        /// Template id rejected.
        /// </summary>
        public string TemplateIdRejected { get; set; }
    }
}