﻿using System;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Pagination request.
    /// </summary>
    public class PaginationRequest
    {
        /// <summary>
        /// Per page.
        /// </summary>
        [DefaultValue(10)]
        [Range(1, int.MaxValue)]
        public int PerPage { get; set; } = 10;

        /// <summary>
        /// Page.
        /// </summary>
        [DefaultValue(1)]
        [Range(1, int.MaxValue)]
        public int Page { get; set; } = 1;

        /// <summary>
        /// Sort by.
        /// </summary>
        public string SortBy { get; set; }

        /// <summary>
        /// Sort direction.
        /// </summary>
        [DefaultValue(NotificationSortDirection.Desc)]
        public NotificationSortDirection SortDirection { get; set; } = NotificationSortDirection.Desc;

        /// <summary>
        /// Direction.
        /// </summary>
        [DefaultValue(NotificationDirection.Incoming)]
        public NotificationDirection Direction { get; set; } = NotificationDirection.Incoming;

        /// <summary>
        /// Get sort by direction.
        /// </summary>
        public string GetSortByDirection()
        {
            return string.IsNullOrEmpty(SortBy) ? default : $"{SortBy}:{SortDirection}".ToUpperInvariant();
        }
    }
}