﻿using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notification request.
    /// </summary>
    public class NotificationRequest
    {
        /// <summary>
        /// Body.
        /// </summary>
        [Required]
        public string Body { get; set; }

        /// <summary>
        /// Type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        [Required]
        public string Subject { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        [DefaultValue("-")]
        public string Answer { get; set; } = "-";

        /// <summary>
        /// Forward to user name.
        /// </summary>
        public string UserNameForwardTo { get; set; }

        /// <summary>
        /// Attachments.
        /// </summary>
        public IEnumerable<NotificationAttachmentRequest> Attachments { get; set; }
    }
}