﻿using System.ComponentModel.DataAnnotations;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notification rule user request.
    /// </summary>
    public class NotificationRuleUserRequest
    {
        /// <summary>
        /// User id.
        /// </summary>
        [Required]
        public string UserId { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        [Required]
        public string UserName { get; set; }
    }
}