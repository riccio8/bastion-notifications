﻿using System.Collections.Generic;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notifications filter request.
    /// </summary>
    public class NotificationsFilterRequest
    {
        /// <summary>
        /// Type.
        /// </summary>
        public ICollection<string> Type { get; set; }

        /// <summary>
        /// Read.
        /// </summary>
        public int? Read { get; set; }

        /// <summary>
        /// Unread.
        /// </summary>
        public int? Unread { get; set; }

        /// <summary>
        /// Pending.
        /// </summary>
        public int? Pending { get; set; }

        /// <summary>
        /// Approved.
        /// </summary>
        public int? Approved { get; set; }

        /// <summary>
        /// Rejected.
        /// </summary>
        public int? Rejected { get; set; }

        /// <summary>
        /// Completed.
        /// </summary>
        public int? Completed { get; internal set; }

        /// <summary>
        /// Search.
        /// </summary>
        public string Search { get; set; }
    }
}