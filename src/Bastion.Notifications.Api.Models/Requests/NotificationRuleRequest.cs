﻿using Bastion.Notifications.Api.Models.Base;

using System.ComponentModel.DataAnnotations;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notification rule request.
    /// </summary>
    public class NotificationRuleRequest : NotificationRuleBase
    {
        /// <summary>
        /// Rule id.
        /// </summary>
        [Required]
        public override string RuleId { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        [Required]
        public override string Name { get; set; }
    }
}