﻿namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notification sort direction.
    /// </summary>
    public enum NotificationSortDirection
    {
        /// <summary>
        /// Asc.
        /// </summary>
        Asc,

        /// <summary>
        /// Desc
        /// </summary>
        Desc,
    }
}