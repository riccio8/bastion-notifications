﻿using System.ComponentModel.DataAnnotations;

namespace Bastion.Notifications.Api.Models.Requests
{
    /// <summary>
    /// Notification attachment request.
    /// </summary>
    public class NotificationAttachmentRequest
    {
        /// <summary>
        /// Id.
        /// </summary>
        [Required]
        public string Id { get; set; }
    }
}