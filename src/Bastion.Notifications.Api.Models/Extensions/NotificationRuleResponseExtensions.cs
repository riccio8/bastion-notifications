﻿using Bastion.Notifications.Api.Models.Responses;
using Bastion.Notifications.Stores;

using System.Linq;

namespace Bastion.Notifications.Api.Models.Extensions
{
    /// <summary>
    /// Notification rule response extensions.
    /// </summary>
    public static class NotificationRuleResponseExtensions
    {
        /// <summary>
        /// Rule to response.
        /// </summary>
        /// <param name="rule">Notification rule.</param>
        public static NotificationRuleResponse ToResponse(this NotificationRule rule)
        {
            return new NotificationRuleResponse
            {
                Name = rule.Name,
                RuleId = rule.NotificationRuleId,
                RuleUsers = rule.Users?.Select(x => x.ToResponse()),
            };
        }

        /// <summary>
        /// User to response.
        /// </summary>
        /// <param name="user">User rule.</param>
        public static NotificationRuleUserResponse ToResponse(this NotificationRuleUser user)
        {
            return new NotificationRuleUserResponse
            {
                UserId = user.User.UserId,
                UserName = user.User.UserName,
                RuleId = user.NotificationRuleId,
            };
        }
    }
}