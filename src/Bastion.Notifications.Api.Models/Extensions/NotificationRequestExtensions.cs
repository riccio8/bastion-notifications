﻿using Bastion.Notifications.Api.Models.Requests;
using Bastion.Notifications.Constants;

namespace Bastion.Notifications.Api.Models.Extensions
{
    /// <summary>
    /// Request extensions.
    /// </summary>
    public static class NotificationRequestExtensions
    {
        /// <summary>
        /// Filter request to is read.
        /// </summary>
        /// <param name="request">Notifications filter request.</param>
        public static bool? ToIsRead(this NotificationsFilterRequest request)
        {
            return request?.Read == request?.Unread ? default : request.Read == 1 ? true : request.Unread == 1 ? false : default(bool?);
        }

        /// <summary>
        /// Filter request to answer id.
        /// </summary>
        /// <param name="request">Notifications filter request.</param>
        public static string ToAnswers(this NotificationsFilterRequest request)
        {
            var answers = string.Empty;
            if (request?.Rejected == 1)
            {
                answers += NotificationAnswerConstants.Reject;
            }

            if (request?.Approved == 1)
            {
                answers += NotificationAnswerConstants.Approve;
            }

            if (request?.Pending == 1)
            {
                answers += NotificationAnswerConstants.Pending;
            }

            if (request?.Completed == 1)
            {
                answers += NotificationAnswerConstants.Completed;
            }

            return answers;
        }

        /// <summary>
        /// To user by direction.
        /// </summary>
        /// <param name="request">Pagination request.</param>
        /// <param name="direction">Direction.</param>
        /// <param name="userId">User id.</param>
        /// <returns>UserId.</returns>
        public static string ToUserByDirection(this PaginationRequest request, NotificationDirection direction, string userId)
        {
            return request.Direction == NotificationDirection.Both || request.Direction == direction ? userId : default;
        }
    }
}