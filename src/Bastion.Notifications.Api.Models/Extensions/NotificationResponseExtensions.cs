﻿using Bastion.Notifications.Api.Models.Responses;
using Bastion.Notifications.Stores;

using System.Linq;

namespace Bastion.Notifications.Api.Models.Extensions
{
    /// <summary>
    /// Notification response extensions.
    /// </summary>
    public static class NotificationResponseExtensions
    {
        /// <summary>
        /// To id response.
        /// </summary>
        /// <param name="notification">Notification.</param>
        public static NotificationIdResponse ToIdResponse(this Notification notification)
        {
            return new NotificationIdResponse
            {
                Id = notification.NotificationId
            };
        }

        /// <summary>
        /// To short response.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="userId">User id.</param>
        public static NotificationShortResponse ToShortResponse(this Notification notification, string userId)
        {
            return new NotificationShortResponse
            {
                Type = notification.Type,
                SentTs = notification.SentAt,
                AnswerTs = notification.AnswerAt,
                Subject = notification.Subject,
                Id = notification.NotificationId,
                ToUserName = notification.ToUser.UserName,
                FromUserName = notification.FromUser.UserName,
                AnswerId = notification.NotificationAnswerId,
                PreviousAnswerId = notification.PreviousAnswerId,
                IsRead = userId == notification.ToUser.UserId ? notification.IsRead : true,
                UserName = userId == notification.ToUser.UserId ? notification.FromUser.UserName : notification.ToUser.UserName,
                Direction = userId == notification.ToUser.UserId ? NotificationDirection.Incoming : NotificationDirection.Outgoing,
            };
        }

        /// <summary>
        /// To detail response.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="userId">User id.</param>
        public static NotificationDetailResponse ToDetailResponse(this Notification notification, string userId)
        {
            return new NotificationDetailResponse
            {
                Body = notification.Body,
                Type = notification.Type,
                SentTs = notification.SentAt,
                AnswerTs = notification.AnswerAt,
                Subject = notification.Subject,
                Id = notification.NotificationId,
                ToUserId = notification.ToUser.UserId,
                ToUserName = notification.ToUser.UserName,
                FromUserId = notification.FromUser.UserId,
                FromUserName = notification.FromUser.UserName,
                AnswerId = notification.NotificationAnswerId,
                PreviousId = notification.PreviousNotificationId,
                AnswerNotificationId = notification.AnswerNotificationId,
                PreviousAnswerId = notification.PreviousAnswerId,
                IsRead = userId == notification.ToUser.UserId ? notification.IsRead : true,
                Attachments = notification.Attachments?.Select(x => x.NotificationAttachmentId)?.ToList(),
                UserName = userId == notification.ToUser.UserId ? notification.FromUser.UserName : notification.ToUser.UserName,
                Direction = userId == notification.ToUser.UserId ? NotificationDirection.Incoming : NotificationDirection.Outgoing,
            };
        }
    }
}