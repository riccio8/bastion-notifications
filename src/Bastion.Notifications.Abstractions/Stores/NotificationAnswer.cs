﻿using System.Collections.Generic;

namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification answer.
    /// </summary>
    public partial class NotificationAnswer
    {
        /// <summary>
        /// Notification answer id.
        /// </summary>
        public char NotificationAnswerId { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Notification answers.
        /// </summary>
        public virtual ICollection<Notification> NotificationAnswers { get; set; } = new HashSet<Notification>();

        /// <summary>
        /// Notification previous answers.
        /// </summary>
        public virtual ICollection<Notification> NotificationPreviousAnswers { get; set; } = new HashSet<Notification>();
    }
}