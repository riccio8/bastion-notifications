﻿namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification attachment.
    /// </summary>
    public partial class NotificationAttachment
    {
        /// <summary>
        /// Notification attachment id.
        /// </summary>
        public string NotificationAttachmentId { get; set; }

        /// <summary>
        /// Notification id.
        /// </summary>
        public string NotificationId { get; set; }

        /// <summary>
        /// Notification.
        /// </summary>
        public virtual Notification Notification { get; set; }

        /// <summary>
        /// Notification file.
        /// </summary>
        public virtual NotificationFile NotificationFile { get; set; }
    }
}