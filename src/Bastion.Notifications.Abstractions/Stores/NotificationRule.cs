﻿using System.Collections.Generic;

namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification rule.
    /// </summary>
    public partial class NotificationRule
    {
        /// <summary>
        /// Rule id.
        /// </summary>
        public string NotificationRuleId { get; set; }

        /// <summary>
        /// Name.
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Template id for pending.
        /// </summary>
        public string TemplateIdPending { get; set; }

        /// <summary>
        /// Template id for rejected.
        /// </summary>
        public string TemplateIdRejected { get; set; }

        /// <summary>
        /// Template id for approved
        /// </summary>
        public string TemplateIdApproved { get; set; }

        /// <summary>
        /// Users for rule.
        /// </summary>
        public virtual ICollection<NotificationRuleUser> Users { get; set; } = new HashSet<NotificationRuleUser>();
    }
}