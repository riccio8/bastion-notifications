﻿namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification rule user.
    /// </summary>
    public partial class NotificationRuleUser
    {
        /// <summary>
        /// Notification rule id.
        /// </summary>
        public string NotificationRuleId { get; set; }

        /// <summary>
        /// User.
        /// </summary>
        public NotificationUser User { get; set; }

        /// <summary>
        /// Rule.
        /// </summary>
        public virtual NotificationRule NotificationRule { get; set; }
    }
}