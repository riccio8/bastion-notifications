﻿using System.Collections.Generic;
using System.Collections.ObjectModel;

namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Paginated list.
    /// </summary>
    /// <typeparam name="T">The list type.</typeparam>
    public class PaginatedList<T> : ReadOnlyCollection<T>
    {
        /// <summary>
        /// Count total.
        /// </summary>
        public int CountTotal { get; }

        /// <summary>
        /// Page index.
        /// </summary>
        public int PageIndex { get; }

        /// <summary>
        ///
        /// </summary>
        public int PageCount { get; }

        /// <summary>
        /// Page size.
        /// </summary>
        public int PageSize { get; }

        /// <summary>
        /// Initializes a new instance of the <see cref="PaginatedList{T}"/> class.
        /// </summary>
        public PaginatedList(IList<T> items, int totalCount, int pageIndex, int pageSize) : base(items)
        {
            PageSize = pageSize;
            PageIndex = pageIndex;
            CountTotal = totalCount;
            PageCount = totalCount / pageSize + (totalCount % pageSize > 0 ? 1 : 0);
        }
    }
}