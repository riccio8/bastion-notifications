﻿namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification user.
    /// </summary>
    public class NotificationUser
    {
        /// <summary>
        /// User id .
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// User name.
        /// </summary>
        public string UserName { get; set; }
    }
}