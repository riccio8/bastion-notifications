﻿using System;

namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification file.
    /// </summary>
    public partial class NotificationFile
    {
        /// <summary>
        /// Notification attachment id.
        /// </summary>
        public string NotificationAttachmentId { get; set; }

        /// <summary>
        /// User id.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// File extension.
        /// </summary>
        public string FileExtension { get; set; }

        /// <summary>
        /// Content type.
        /// </summary>
        public string ContentType { get; set; }

        /// <summary>
        /// File upload at
        /// </summary>
        public DateTime? FileUploadAt { get; set; }

        /// <summary>
        /// File data.
        /// </summary>
        public byte[] FileData { get; set; }

        /// <summary>
        /// Notification attachment.
        /// </summary>
        public virtual NotificationAttachment NotificationAttachment { get; set; }
    }
}