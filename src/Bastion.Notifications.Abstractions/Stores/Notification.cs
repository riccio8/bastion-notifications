﻿using System;
using System.Collections.Generic;

namespace Bastion.Notifications.Stores
{
    /// <summary>
    /// Notification.
    /// </summary>
    public partial class Notification
    {
        /// <summary>
        /// Notification id.
        /// </summary>
        public string NotificationId { get; set; }

        /// <summary>
        /// Answer notification id.
        /// </summary>
        public string AnswerNotificationId { get; set; }

        /// <summary>
        /// Previous notification id.
        /// </summary>
        public string PreviousNotificationId { get; set; }

        /// <summary>
        /// From user.
        /// </summary>
        public NotificationUser FromUser { get; set; }

        /// <summary>
        /// To user.
        /// </summary>
        public NotificationUser ToUser { get; set; }

        /// <summary>
        /// Sent at.
        /// </summary>
        public DateTime SentAt { get; set; }

        /// <summary>
        /// Sent at.
        /// </summary>
        public DateTime? AnswerAt { get; set; }

        /// <summary>
        /// Type.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Subject.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Body.
        /// </summary>
        public string Body { get; set; }

        /// <summary>
        /// Is read.
        /// </summary>
        public bool IsRead { get; set; }

        /// <summary>
        /// Notification answer id.
        /// </summary>
        public char NotificationAnswerId { get; set; }

        /// <summary>
        /// Previous notification answer id.
        /// </summary>
        public char? PreviousAnswerId { get; set; }

        /// <summary>
        /// Notification answer.
        /// </summary>
        public virtual NotificationAnswer NotificationAnswer { get; set; }

        /// <summary>
        /// Notification previous answer.
        /// </summary>
        public virtual NotificationAnswer NotificationPreviousAnswer { get; set; }

        /// <summary>
        /// Attachments.
        /// </summary>
        public virtual ICollection<NotificationAttachment> Attachments { get; set; } = new HashSet<NotificationAttachment>();
    }
}