﻿using Bastion.Notifications.Stores;

using System.Collections.Generic;
using System.Threading.Tasks;

namespace Bastion.Notifications.Abstractions
{
    /// <summary>
    /// Notifications service.
    /// </summary>
    public interface INotificationsService
    {
        /// <summary>
        /// Send to user the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="userId">The user Id.</param>
        /// <returns>The task.</returns>
        Task SendToUserAsync<TMessage>(
            TMessage message,
            string userId)
            where TMessage : INotifiable;

        /// <summary>
        /// Send to group the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="group">The group.</param>
        /// <returns>The task.</returns>
        Task SendToGroupAsync<TMessage>(
            TMessage message,
            string group)
            where TMessage : INotifiable;

        /// <summary>
        /// Send notifications unread count.
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>The task.</returns>
        Task SendUnreadCountAsync(
            string userId);

        /// <summary>
        /// Send notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>The task.</returns>
        Task SendAsync(
            Notification notification);

        /// <summary>
        /// Set all notifications as read for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>List of modified notifications.</returns>
        Task<List<Notification>> SetNotificationsAsReadAsync(
            string userId);

        /// <summary>
        /// Set notification as read.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>Modified notification.</returns>
        Task<Notification> SetNotificationAsReadAsync(
            Notification notification);

        /// <summary>
        /// Change notification subject.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="subject">Notification subject.</param>
        /// <returns>Modified notification.</returns>
        Task<Notification> ChangeNotificationSubjectAsync(Notification notification, string subject);

        /// <summary>
        /// Delete notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>Deleted notification.</returns>
        Task<Notification> DeleteAsync(
            Notification notification);

        /// <summary>
        /// Get all notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>The task list of <see cref="Notification"/>.</returns>
        Task<List<Notification>> GetNotificationsAsync(
            string userId);

        /// <summary>
        /// Get notifications for user.
        /// </summary>
        /// <param name="fromUserId">From user id.</param>
        /// <param name="toUserId">To user id.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetNotificationsAsync(
            string fromUserId,
            string toUserId,
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize);

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetNotificationsAsync(
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default);

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="types">Types notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetNotificationsAsync(
            string search,
            bool? isRead,
            string answers,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> types = default,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default);

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <returns>The task.</returns>
        Task<Notification> GetNotificationAsync(
            string notificationId);

        /// <summary>
        /// Get notification by id for user.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userId">User id.</param>
        /// <returns>The task.</returns>
        Task<Notification> GetNotificationAsync(
            string userId,
            string notificationId);

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        Task<Notification> AddAsync(
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments);

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        Task<Notification> AddWithIdAsync(
            string notificationId,
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments);

        /// <summary>
        /// Add notification with id by rule.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        Task<Notification> AddWithIdByRuleAsync(
            string notificationId,
            NotificationUser userFrom,
            string ruleId,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments);

        /// <summary>
        /// Add notifications by rule.
        /// </summary>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        Task<ICollection<Notification>> AddByRuleAsync(
            NotificationUser userFrom,
            string ruleId,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments);

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userForwardTo">To forward user.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <param name="type">Notification type.</param>
        /// <returns>The task.</returns>
        Task<Notification> AddAnswerAsync(
            string notificationId,
            char answerId,
            NotificationUser userFrom,
            string subject,
            string body,
            IEnumerable<string> attachments,
            NotificationUser userForwardTo = default,
            string type = default);

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="userForwardTo">To forward user.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <param name="type">Notification type.</param>
        /// <returns>The task.</returns>
        Task<Notification> AddAnswerAsync(
            Notification notification,
            char answerId,
            string subject,
            string body,
            IEnumerable<string> attachments,
            NotificationUser userForwardTo = default,
            string type = default);

        /// <summary>
        /// Add file.
        /// </summary>
        /// <param name="notificationFile">Notification file.</param>
        Task<NotificationFile> AddFileAsync(
            NotificationFile notificationFile);

        /// <summary>
        /// Get file by id.
        /// </summary>
        /// <param name="notificationAttachmentId">Notification attachment id.</param>
        Task<NotificationFile> GetFileAsync(
            string notificationAttachmentId);

        /// <summary>
        /// Add attachment.
        /// </summary>
        /// <param name="notificationAttachment">Notification attachment.</param>
        Task<NotificationAttachment> AddAttachmentAsync(
            NotificationAttachment notificationAttachment);

        /// <summary>
        /// Add attachments.
        /// </summary>
        /// <param name="notificationAttachments">Notification attachments.</param>
        Task<IEnumerable<NotificationAttachment>> AddAttachmentsAsync(
            IEnumerable<NotificationAttachment> notificationAttachments);
    }
}