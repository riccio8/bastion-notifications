﻿using System.Text.Json.Serialization;

namespace Bastion.Notifications.Abstractions
{
    /// <summary>
    /// Represents an interface than can be notified via hub.
    /// </summary>
    public interface INotifiable
    {
        /// <summary>
        /// Gets SignalR method.
        /// </summary>
        [JsonIgnore]
        string NotificationMethod { get; }
    }
}