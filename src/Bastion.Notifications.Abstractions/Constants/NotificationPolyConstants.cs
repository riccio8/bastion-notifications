﻿namespace Bastion.Notifications.Constants
{
    /// <summary>
    /// Notification policy constants.
    /// </summary>
    public static class NotificationPolicyConstants
    {
        public const string ManagementPolicy = "NotificationManagementPolicy";
    }
}