﻿namespace Bastion.Notifications.Constants
{
    /// <summary>
    /// Notification answer constants.
    /// </summary>
    public static class NotificationAnswerConstants
    {
        /// <summary>
        /// No answer.
        /// </summary>
        public const char NoAnswer = '-';

        /// <summary>
        /// Approve.
        /// </summary>
        public const char Approve = 'A';

        /// <summary>
        /// Reject.
        /// </summary>
        public const char Reject = 'R';

        /// <summary>
        /// Forward.
        /// </summary>
        public const char Forward = 'F';

        /// <summary>
        /// Approve and Forward.
        /// </summary>
        public const char More = 'M';

        /// <summary>
        /// Pending complaince.
        /// </summary>
        public const char Pending = 'P';

        /// <summary>
        /// Processing.
        /// </summary>
        public const char Processing = 'S';

        /// <summary>
        /// Completed.
        /// </summary>
        public const char Completed = 'C';

        /// <summary>
        /// Pending withdraw.
        /// </summary>
        public const char PendingWithdraw = 'W';

        /// <summary>
        /// Money in our bank.
        /// </summary>
        public const char MoneyInOurBank = 'B';

        /// <summary>
        /// Money traveling to our account.
        /// </summary>
        public const char MoneyTraveling = 'T';
    }
}