﻿using Bastion.Notifications.Stores;

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bastion.Notifications.Abstractions
{
    /// <summary>
    /// Notifications repository.
    /// </summary>
    public interface INotificationsRepository
    {
        /// <summary>
        /// Get сount unread notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of Count unread notifications.</returns>
        Task<int> GetCountUnreadAsync(
            string userId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        Task<Notification> GetByIdAsync(
            string notificationId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notification by id for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        Task<Notification> GetAsync(
            string userId,
            string notificationId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notification by id for users.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userIds">Collection of user id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        Task<Notification> GetAsync(
            IEnumerable<string> userIds,
            string notificationId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get all notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task list of <see cref="Notification"/>.</returns>
        Task<List<Notification>> GetAsync(
            string userId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notifications for user.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserId">From user id.</param>
        /// <param name="toUserId">To user id.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetPaginatedAsync(
            string fromUserId,
            string toUserId,
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetPaginatedAsync(
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="types">Types notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        Task<PaginatedList<Notification>> GetPaginatedAsync(
            string search,
            bool? isRead,
            string answers,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> types = default,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Added notification.</returns>
        Task<Notification> AddAsync(
            Notification notification,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add notifications.
        /// </summary>
        /// <param name="notifications">Notifications.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Added notifications.</returns>
        Task<ICollection<Notification>> AddAsync(
            ICollection<Notification> notifications,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Delete notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Deleted notification.</returns>
        Task<Notification> DeleteAsync(
            Notification notification,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Mark as read all notifications for the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>List of modified notifications.</returns>
        Task<List<Notification>> MarkAsReadAsync(
            string userId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Mark as read notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Modified notification.</returns>
        Task<Notification> SetNotificationAsReadAsync(
            Notification notification,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Change notification subject.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Modified notification.</returns>
        Task<Notification> ChangeNotificationSubjectAsync(
            Notification notification, 
            string subject,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add file.
        /// </summary>
        /// <param name="notificationFile">Notification file.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        Task<NotificationFile> AddFileAsync(
            NotificationFile notificationFile,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get file by id.
        /// </summary>
        /// <param name="notificationAttachmentId">Notification attachment id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        Task<NotificationFile> GetFileAsync(
            string notificationAttachmentId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add attachment.
        /// </summary>
        /// <param name="notificationAttachment">Notification attachment.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        Task<NotificationAttachment> AddAttachmentAsync(
            NotificationAttachment notificationAttachment,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add attachments.
        /// </summary>
        /// <param name="notificationAttachments">Notification attachments.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        Task<IEnumerable<NotificationAttachment>> AddAttachmentsAsync(
            IEnumerable<NotificationAttachment> notificationAttachments,
            CancellationToken cancellationToken = default);
    }
}