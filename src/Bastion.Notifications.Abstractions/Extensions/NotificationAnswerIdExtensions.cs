﻿using Bastion.Notifications.Constants;

namespace Bastion.Notifications.Extensions
{
    public static class NotificationAnswerIdExtensions
    {
        public static string AnswerIdToString(char answerId)
        {
            switch (answerId)
            {
                case NotificationAnswerConstants.NoAnswer:
                    return "No answer";

                case NotificationAnswerConstants.Approve:
                    return "Approve";

                case NotificationAnswerConstants.Reject:
                    return "Reject";

                case NotificationAnswerConstants.Forward:
                    return "Forward";

                case NotificationAnswerConstants.More:
                    return "Approve and Forward";

                case NotificationAnswerConstants.Pending:
                    return "Pending complaince";

                case NotificationAnswerConstants.Processing:
                    return "Processing";

                case NotificationAnswerConstants.Completed:
                    return "Completed";

                case NotificationAnswerConstants.PendingWithdraw:
                    return "Pending withdraw";

                case NotificationAnswerConstants.MoneyInOurBank:
                    return "Money in our bank";

                case NotificationAnswerConstants.MoneyTraveling:
                    return "Money traveling to our account";

                default:
                    return "Unknow";
            }
        }
    }
}