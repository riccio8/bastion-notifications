﻿using Microsoft.Extensions.DependencyInjection;

namespace Bastion.Notifications.Extensions
{
    /// <summary>
    /// Helper functions for configuring notifications services.
    /// </summary>
    public class NotificationsBuilder
    {
        /// <summary>
        /// Creates a new instance of <see cref="NotificationsBuilder"/>.
        /// </summary>
        /// <param name="services">The <see cref="IServiceCollection"/> to attach to.</param>
        public NotificationsBuilder(IServiceCollection services)
        {
            Services = services;
        }

        /// <summary>
        /// Gets the <see cref="IServiceCollection"/> services are attached to.
        /// </summary>
        /// <value>
        /// The <see cref="IServiceCollection"/> services are attached to.
        /// </value>
        public IServiceCollection Services { get; private set; }
    }
}