﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Stores;

using Microsoft.AspNetCore.Http;

using System;
using System.Collections.Generic;
using System.IO;
using System.Text.Json;
using System.Threading.Tasks;

namespace Bastion.Notifications.Extensions
{
    /// <summary>
    /// Notifications service extensions.
    /// </summary>
    public static class NotificationsServiceExtensions
    {
        private static readonly JsonSerializerOptions options = new JsonSerializerOptions()
        {
            IgnoreNullValues = true,
            PropertyNameCaseInsensitive = true,
            PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
        };

        public static string ToJson<TBody>(TBody body) where TBody : class
        {
            return JsonSerializer.Serialize(body, options);
        }

        /// <summary>
        /// Add notification to user.
        /// </summary>
        /// <typeparam name="TBody"></typeparam>
        /// <param name="notificationsService"></param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public static Task<Notification> AddAsync<TBody>(
            this INotificationsService notificationsService,
            string subject,
            TBody body,
            char answerId,
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            IEnumerable<string> attachments = default) where TBody : class
        {
            return notificationsService.AddAsync(userFrom, userTo, type, subject, ToJson(body), answerId, attachments);
        }

        /// <summary>
        /// Add notification to user.
        /// </summary>
        /// <typeparam name="TBody"></typeparam>
        /// <param name="notificationsService"></param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Answer id.</param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public static Task<Notification> AddWithIdAsync<TBody>(
            this INotificationsService notificationsService,
            string notificationId,
            string subject,
            TBody body,
            char answerId,
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            IEnumerable<string> attachments = default) where TBody : class
        {
            return notificationsService.AddWithIdAsync(notificationId, userFrom, userTo, type, subject, ToJson(body), answerId, attachments);
        }

        /// <summary>
        /// Add with id by rule.
        /// </summary>
        /// <typeparam name="TBody"></typeparam>
        /// <param name="notificationsService"></param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public static Task<Notification> AddWithIdByRuleAsync<TBody>(
            this INotificationsService notificationsService,
            string notificationId,
            string subject,
            TBody body,
            char answerId,
            NotificationUser userFrom,
            string ruleId,
            IEnumerable<string> attachments) where TBody : class
        {
            return notificationsService.AddWithIdByRuleAsync(notificationId, userFrom, ruleId, subject, ToJson(body), answerId, attachments);
        }

        /// <summary>
        /// Add notification by rule id.
        /// </summary>
        /// <typeparam name="TBody"></typeparam>
        /// <param name="notificationsService"></param>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public static Task<ICollection<Notification>> AddByRuleAsync<TBody>(
            this INotificationsService notificationsService,
            string subject,
            TBody body,
            char answerId,
            NotificationUser userFrom,
            string ruleId,
            IEnumerable<string> attachments = default) where TBody : class
        {
            return notificationsService.AddByRuleAsync(userFrom, ruleId, subject, ToJson(body), answerId, attachments);
        }

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <typeparam name="TBody"></typeparam>
        /// <param name="notificationsService"></param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userForwardTo">Forward to user.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Notification answer.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public static Task<Notification> AddAnswerAsync<TBody>(
            this INotificationsService notificationsService,
            string notificationId,
            string subject,
            TBody body,
            char answerId,
            NotificationUser userFrom,
            IEnumerable<string> attachments = default,
            NotificationUser userForwardTo = default,
            string type = default) where TBody : class
        {
            return notificationsService.AddAnswerAsync(notificationId, answerId, userFrom, subject, ToJson(body), attachments, userForwardTo, type);
        }

        /// <summary>
        /// Add attachment from file.
        /// </summary>
        /// <param name="notificationsService">Notifications service.</param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userId">User id.</param>
        /// <param name="file">File.</param>
        public static Task<NotificationAttachment> AddAttachmentFromFileAsync(
            this INotificationsService notificationsService,
            string notificationId,
            string userId,
            IFormFile file)
        {
            return notificationsService.AddAttachmentAsync(notificationId, userId, file);
        }

        /// <summary>
        /// Add attachment from file.
        /// </summary>
        /// <param name="notificationsService">Notifications service.</param>
        /// <param name="notification">Notification.</param>
        /// <param name="file">File.</param>
        public static Task<NotificationAttachment> AddAttachmentFromFileAsync(
            this INotificationsService notificationsService,
            Notification notification,
            IFormFile file)
        {
            return notificationsService.AddAttachmentFromFileAsync(notification.NotificationId, notification.FromUser.UserId, file);
        }

        private static async Task<NotificationAttachment> AddAttachmentAsync(
            this INotificationsService notificationsService,
            string notificationId,
            string userId,
            IFormFile file)
        {
            using var stream = new MemoryStream();
            await file.CopyToAsync(stream);

            var notificationAttachment = await notificationsService.AddAttachmentAsync(new NotificationAttachment
            {
                NotificationId = notificationId,
                NotificationFile = new NotificationFile
                {
                    UserId = userId,
                    FileData = stream.ToArray(),
                    FileUploadAt = DateTime.UtcNow,
                    ContentType = file.ContentType,
                    FileExtension = Path.GetExtension(file.FileName),
                    NotificationAttachmentId = Guid.NewGuid().ToString(),
                },
            });

            return notificationAttachment;
        }
    }
}