﻿using Bastion.Notifications.Stores;

using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;

namespace Bastion.Notifications.Abstractions
{
    /// <summary>
    /// Notification rules repository.
    /// </summary>
    public interface INotificationRulesRepository
    {
        /// <summary>
        /// Get users for rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <returns>The task of list of <see cref="NotificationRuleUser"/>.</returns>
        Task<List<NotificationRuleUser>> GetUsersForRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add user for rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <param name="userId">User id.</param>
        /// <param name="userName">User name.</param>
        /// <returns>The task of <see cref="NotificationRuleUser"/>.</returns>
        Task<NotificationRuleUser> AddUserForRuleAsync(
            string ruleId,
            string userId,
            string userName,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Delete user from rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <param name="userId">User id.</param>
        /// <returns>The task of <see cref="NotificationRuleUser"/>.</returns>
        Task<NotificationRuleUser> DeleteUserFromRuleAsync(
            string ruleId,
            string userId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notification rules.
        /// </summary>
        /// <returns>The Task of list of <see cref="NotificationRule"/> rules.</returns>
        Task<List<NotificationRule>> GetRulesAsync(
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Get notification rule by id.
        /// </summary>
        /// <param name="ruleId">Rule Id.</param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        Task<NotificationRule> GetRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Add notification rule.
        /// </summary>
        /// <param name="notificationRule"></param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        Task<NotificationRule> AddRuleAsync(
            NotificationRule notificationRule,
            CancellationToken cancellationToken = default);

        /// <summary>
        /// Delete notification rule.
        /// </summary>
        /// <param name="notificationRule"></param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        Task<NotificationRule> DeleteRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default);
    }
}