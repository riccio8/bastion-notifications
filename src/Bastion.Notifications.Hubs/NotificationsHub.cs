﻿using Bastion.Notifications.Abstractions;

using Microsoft.AspNetCore.SignalR;

using System;
using System.Threading.Tasks;

namespace Bastion.Notifications.Hubs
{
    /// <summary>
    /// Notifications hub.
    /// </summary>
    public class NotificationsHub : Hub
    {
        private readonly INotificationsService notificationsService;

        public NotificationsHub(INotificationsService notificationsService)
        {
            this.notificationsService = notificationsService;
        }

        public override async Task OnConnectedAsync()
        {
            await base.OnConnectedAsync();

            await Clients.Caller.SendAsync("Welcome", DateTime.UtcNow);
            await notificationsService.SendUnreadCountAsync(Context.UserIdentifier);
        }
    }
}