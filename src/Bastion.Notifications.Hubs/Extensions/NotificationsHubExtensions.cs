﻿using Bastion.Notifications.Abstractions;

using Microsoft.AspNetCore.SignalR;

using System.Threading.Tasks;

namespace Bastion.Notifications.Hubs.Extensions
{
    /// <summary>
    /// Notifications hub extensions.
    /// </summary>
    public static class NotificationsHubExtensions
    {
        /// <summary>
        /// Send to user the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="userId">The user Id.</param>
        /// <returns>The task.</returns>
        public static Task SendToUserAsync<TMessage, TNotificationsHub>(
            this IHubContext<TNotificationsHub> notificationHub,
            TMessage message,
            string userId)
            where TMessage : INotifiable
            where TNotificationsHub : NotificationsHub
        {
            return notificationHub.Clients.User(userId)
                .SendAsync(message.NotificationMethod, message);
        }

        /// <summary>
        /// Send to group the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="group">The group.</param>
        /// <returns>The task.</returns>
        public static Task SendToGroupAsync<TMessage, TNotificationsHub>(
            this IHubContext<TNotificationsHub> notificationHub,
            TMessage message,
            string group)
            where TMessage : INotifiable
            where TNotificationsHub : NotificationsHub
        {
            return notificationHub.Clients.Group(group)
                .SendAsync(message.NotificationMethod, message);
        }
    }
}