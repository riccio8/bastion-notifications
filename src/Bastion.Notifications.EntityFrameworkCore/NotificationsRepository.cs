﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.EntityFrameworkCore.Extensions;
using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bastion.Notifications.EntityFrameworkCore
{
    /// <summary>
    /// Notifications repository.
    /// </summary>
    internal class NotificationsRepository<TContext> : INotificationsRepository where TContext : NotificationDbContext
    {
        private readonly TContext notificationContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsRepository"/> class.
        /// </summary>
        public NotificationsRepository(TContext notificationContext)
        {
            this.notificationContext = notificationContext;
        }

        /// <summary>
        /// Get сount unread notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of Count unread notifications.</returns>
        public Task<int> GetCountUnreadAsync(
            string userId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Notifications
                .AsNoTracking()
                .CountAsync(notification => notification.ToUser.UserId == userId && !notification.IsRead);
        }

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        public Task<Notification> GetByIdAsync(
            string notificationId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Notifications
                .Include(x => x.Attachments)
                .Include(x => x.NotificationAnswer)
                .AsNoTracking()
                .Where(notification => notification.NotificationId == notificationId)
                .FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        public Task<Notification> GetAsync(
            string userId,
            string notificationId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Notifications
                .Include(x => x.Attachments)
                .Include(x => x.NotificationAnswer)
                .AsNoTracking()
                .Where(notification => notification.NotificationId == notificationId && (notification.ToUser.UserId == userId || notification.FromUser.UserId == userId))
                .FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="userIds">User id.</param>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task of <see cref="Notification"/>.</returns>
        public Task<Notification> GetAsync(
            IEnumerable<string> userIds,
            string notificationId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Notifications
                .Include(x => x.Attachments)
                .Include(x => x.NotificationAnswer)
                .AsNoTracking()
                .Where(notification => notification.NotificationId == notificationId && (userIds.Contains(notification.ToUser.UserId) || userIds.Contains(notification.FromUser.UserId)))
                .FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// Get all notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task list of <see cref="Notification"/>.</returns>
        public Task<List<Notification>> GetAsync(
            string userId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Notifications
                .AsNoTracking()
                .Where(notification => notification.ToUser.UserId == userId)
                .ToListAsync();
        }

        /// <summary>
        /// Get notifications for user.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserId">From user id.</param>
        /// <param name="toUserId">To user id.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetPaginatedAsync(
            string fromUserId,
            string toUserId,
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            CancellationToken cancellationToken = default)
        {
            var notifications = notificationContext.Notifications
                .AsNoTracking();

            if (!string.IsNullOrEmpty(fromUserId) && fromUserId == toUserId)
            {
                notifications = notifications.Where(notification => notification.FromUser.UserId == fromUserId || notification.ToUser.UserId == fromUserId);
            }
            else
            {
                if (!string.IsNullOrEmpty(fromUserId))
                {
                    notifications = notifications.Where(notification => notification.FromUser.UserId == fromUserId);
                }
                if (!string.IsNullOrEmpty(toUserId))
                {
                    notifications = notifications.Where(notification => notification.ToUser.UserId == toUserId);
                }
            }

            if (!string.IsNullOrEmpty(type))
            {
                notifications = notifications.Where(notification => notification.Type.ToUpper().Equals(type));
            }

            return GetPaginatedAsync(notifications, search, isRead, answers, sortByDirection, pageIndex, pageSize, cancellationToken);
        }

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetPaginatedAsync(
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default,
            CancellationToken cancellationToken = default)
        {
            var notifications = notificationContext.Notifications
                .AsNoTracking();

            if (fromUserIds != null && fromUserIds == toUserIds)
            {
                notifications = notifications.Where(notification => fromUserIds.Contains(notification.FromUser.UserId) || toUserIds.Contains(notification.ToUser.UserId));
            }
            else
            {
                if (fromUserIds != null)
                {
                    notifications = notifications.Where(notification => fromUserIds.Contains(notification.FromUser.UserId));
                }
                if (toUserIds != null)
                {
                    notifications = notifications.Where(notification => toUserIds.Contains(notification.ToUser.UserId));
                }
            }

            if (!string.IsNullOrEmpty(type))
            {
                notifications = notifications.Where(notification => notification.Type.ToUpper().Equals(type));
            }

            return GetPaginatedAsync(notifications, search, isRead, answers, sortByDirection, pageIndex, pageSize, cancellationToken);
        }

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="search">Search.</param>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="types">Types notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetPaginatedAsync(
            string search,
            bool? isRead,
            string answers,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> types = default,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default,
            CancellationToken cancellationToken = default)
        {
            var notifications = notificationContext.Notifications
                .AsNoTracking();

            if (fromUserIds != null && fromUserIds == toUserIds)
            {
                notifications = notifications.Where(notification => fromUserIds.Contains(notification.FromUser.UserId) || toUserIds.Contains(notification.ToUser.UserId));
            }
            else
            {
                if (fromUserIds != null)
                {
                    notifications = notifications.Where(notification => fromUserIds.Contains(notification.FromUser.UserId));
                }
                if (toUserIds != null)
                {
                    notifications = notifications.Where(notification => toUserIds.Contains(notification.ToUser.UserId));
                }
            }

            if (types != null)
            {
                notifications = notifications.Where(notification => types.Contains(notification.Type));
            }

            return GetPaginatedAsync(notifications, search, isRead, answers, sortByDirection, pageIndex, pageSize, cancellationToken);
        }

        private static Task<PaginatedList<Notification>> GetPaginatedAsync(IQueryable<Notification> notifications, string search, bool? isRead, string answers, string sortByDirection, int pageIndex, int pageSize, CancellationToken cancellationToken)
        {
            if (isRead.HasValue)
            {
                notifications = notifications.Where(notification => notification.IsRead.Equals(isRead.Value));
            }

            if (!string.IsNullOrEmpty(answers))
            {
                notifications = notifications.Where(notification => Enumerable.Contains(answers, notification.NotificationAnswerId));
            }

            if (!string.IsNullOrEmpty(search))
            {
                var searchUpper = search.ToUpperInvariant();
                notifications = notifications.Where(x
                    => (!string.IsNullOrEmpty(x.NotificationId) && x.NotificationId.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.AnswerNotificationId) && x.AnswerNotificationId.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.PreviousNotificationId) && x.PreviousNotificationId.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.FromUser.UserName) && x.FromUser.UserName.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.ToUser.UserName) && x.ToUser.UserName.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.Subject) && x.Subject.ToUpper().Contains(searchUpper))
                    || (!string.IsNullOrEmpty(x.Body) && x.Body.ToUpper().Contains(searchUpper)));
            }

            notifications = sortByDirection switch
            {
                "ID:ASC" => notifications.OrderBy(notification => notification.NotificationId),
                "ID:DESC" => notifications.OrderByDescending(notification => notification.NotificationId),
                "FROMUSERNAME:ASC" => notifications.OrderBy(notification => notification.FromUser.UserName),
                "FROMUSERNAME:DESC" => notifications.OrderByDescending(notification => notification.FromUser.UserName),
                "SUBJECT:ASC" => notifications.OrderBy(notification => notification.Subject),
                "SUBJECT:DESC" => notifications.OrderByDescending(notification => notification.Subject),
                "SENTTS:ASC" => notifications.OrderBy(notification => notification.SentAt),
                "SENTTS:DESC" => notifications.OrderByDescending(notification => notification.SentAt),
                "ANSWERAT:ASC" => notifications.Where(notification => notification.AnswerAt.HasValue).OrderBy(notification => notification.AnswerAt.Value),
                "ANSWERAT:DESC" => notifications.Where(notification => notification.AnswerAt.HasValue).OrderByDescending(notification => notification.AnswerAt.Value),
                "STATUS:ASC" => notifications.OrderBy(notification => notification.NotificationAnswerId),
                "STATUS:DESC" => notifications.OrderByDescending(notification => notification.NotificationAnswerId),
                _ => notifications.OrderByDescending(notification => notification.SentAt)
            };

            return notifications.ToPaginatedListAsync(pageIndex, pageSize, cancellationToken);
        }

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Added notification.</returns>
        public async Task<Notification> AddAsync(
            Notification notification,
            CancellationToken cancellationToken = default)
        {
            await AddAndUpdate(notification);
            await SaveChangesAsync(cancellationToken);

            return notification;
        }

        /// <summary>
        /// Add notifications.
        /// </summary>
        /// <param name="notifications">Notifications.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Added notifications.</returns>
        public async Task<ICollection<Notification>> AddAsync(
            ICollection<Notification> notifications,
            CancellationToken cancellationToken = default)
        {
            await Task.WhenAll(notifications.Select(async notification => await AddAndUpdate(notification)).ToArray());

            await SaveChangesAsync(cancellationToken);

            return notifications;
        }

        private async Task<Notification> AddAndUpdate(Notification notification)
        {
            notificationContext.Notifications.Add(notification);
            if (notification.PreviousAnswerId.HasValue && !string.IsNullOrEmpty(notification.PreviousNotificationId))
            {
                var previousNotification = await notificationContext.Notifications
                    .FirstOrDefaultAsync(x => x.NotificationId == notification.PreviousNotificationId);
                if (previousNotification != null)
                {
                    previousNotification.AnswerAt = notification.SentAt;
                    previousNotification.AnswerNotificationId = notification.NotificationId;
                    previousNotification.NotificationAnswerId = notification.PreviousAnswerId.Value;
                    notificationContext.Notifications.Update(previousNotification);
                }
            }

            return notification;
        }

        /// <summary>
        /// Delete notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Deleted notification.</returns>
        public async Task<Notification> DeleteAsync(
            Notification notification,
            CancellationToken cancellationToken = default)
        {
            notificationContext.Notifications.Remove(notification);

            await SaveChangesAsync(cancellationToken);

            return notification;
        }

        /// <summary>
        /// Mark as read all notifications for the user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>List of modified notifications.</returns>
        public async Task<List<Notification>> MarkAsReadAsync(
            string userId,
            CancellationToken cancellationToken = default)
        {
            var notifications = await notificationContext.Notifications
                .AsNoTracking()
                .Where(notification => notification.ToUser.UserId == userId && !notification.IsRead)
                .ToListAsync();

            if (notifications.Count > 0)
            {
                notifications.ForEach(x => x.IsRead = true);
                notificationContext.Notifications.UpdateRange(notifications);

                await SaveChangesAsync(cancellationToken);
            }

            return notifications;
        }

        /// <summary>
        /// Mark as read notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Modified notification.</returns>
        public async Task<Notification> SetNotificationAsReadAsync(
            Notification notification,
            CancellationToken cancellationToken = default)
        {
            if (!notification.IsRead)
            {
                notification.IsRead = true;
                notificationContext.Notifications.Update(notification);

                await SaveChangesAsync(cancellationToken);
            }
            return notification;
        }

        /// <summary>
        /// Change notification subject.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        /// <returns>Modified notification.</returns>
        public async Task<Notification> ChangeNotificationSubjectAsync(
            Notification notification,
            string subject,
            CancellationToken cancellationToken = default)
        {
            notification.Subject = subject;
            notificationContext.Notifications.Update(notification);

            await SaveChangesAsync(cancellationToken);

            return notification;
        }

        /// <summary>
        /// Add file.
        /// </summary>
        /// <param name="notificationFile">Notification file.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        public async Task<NotificationFile> AddFileAsync(
            NotificationFile notificationFile,
            CancellationToken cancellationToken = default)
        {
            notificationContext.Files.Add(notificationFile);

            await SaveChangesAsync(cancellationToken);

            return notificationFile;
        }

        /// <summary>
        /// Get file by id.
        /// </summary>
        /// <param name="notificationAttachmentId">Notification attachment id.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        public Task<NotificationFile> GetFileAsync(
            string notificationAttachmentId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Files
                .AsNoTracking()
                .Where(x => x.NotificationAttachmentId == notificationAttachmentId)
                .FirstOrDefaultAsync(cancellationToken);
        }

        /// <summary>
        /// Add attachment.
        /// </summary>
        /// <param name="notificationAttachment">Notification attachment.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        public async Task<NotificationAttachment> AddAttachmentAsync(
            NotificationAttachment notificationAttachment,
            CancellationToken cancellationToken = default)
        {
            notificationContext.Attachments.Add(notificationAttachment);

            await SaveChangesAsync(cancellationToken);

            return notificationAttachment;
        }

        /// <summary>
        /// Add attachments.
        /// </summary>
        /// <param name="notificationAttachments">Notification attachments.</param>
        /// <param name="cancellationToken">A <see cref="CancellationToken"/> to observe while waiting for the task to complete.</param>
        public async Task<IEnumerable<NotificationAttachment>> AddAttachmentsAsync(
            IEnumerable<NotificationAttachment> notificationAttachments,
            CancellationToken cancellationToken = default)
        {
            notificationContext.Attachments.AddRange(notificationAttachments);

            await SaveChangesAsync(cancellationToken);

            return notificationAttachments;
        }

        private protected Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return notificationContext.SaveChangesAsync(cancellationToken);
        }
    }
}