﻿using Bastion.Notifications.EntityFrameworkCore.Configurations;
using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;

namespace Bastion.Notifications.EntityFrameworkCore
{
    /// <summary>
    /// Notification database context.
    /// </summary>
    public partial class NotificationDbContext : DbContext
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationDbContext"/> class.
        /// </summary>
        public NotificationDbContext(DbContextOptions<NotificationDbContext> options)
            : base(options)
        {
        }

        /// <summary>
        /// Files.
        /// </summary>
        public virtual DbSet<NotificationFile> Files { get; set; }

        /// <summary>
        /// Rules.
        /// </summary>
        public virtual DbSet<NotificationRule> Rules { get; set; }

        /// <summary>
        /// Notifications.
        /// </summary>
        public virtual DbSet<Notification> Notifications { get; set; }

        /// <summary>
        /// Answers.
        /// </summary>
        public virtual DbSet<NotificationAnswer> Answers { get; set; }

        /// <summary>
        /// Rule users.
        /// </summary>
        public virtual DbSet<NotificationRuleUser> RuleUsers { get; set; }

        /// <summary>
        /// Attachments.
        /// </summary>
        public virtual DbSet<NotificationAttachment> Attachments { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);

            modelBuilder.ApplyConfiguration(new NotificationConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationFileConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationRuleConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationAnswerConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationRuleUserConfiguration());
            modelBuilder.ApplyConfiguration(new NotificationAttachmentConfiguration());
        }
    }
}