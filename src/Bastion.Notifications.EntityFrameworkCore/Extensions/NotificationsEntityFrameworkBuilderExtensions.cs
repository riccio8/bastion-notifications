﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.EntityFrameworkCore;
using Bastion.Notifications.Extensions;

using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="NotificationsBuilder"/> for adding entity framework repositories.
    /// </summary>
    public static class NotificationsEntityFrameworkBuilderExtensions
    {
        /// <summary>
        /// Adds an Entity Framework implementation of notifications information repositories.
        /// </summary>
        /// <typeparam name="TContext">The Entity Framework database context to use.</typeparam>
        /// <param name="builder">The <see cref="NotificationsBuilder"/> instance this method extends.</param>
        /// <returns>The <see cref="NotificationsBuilder"/> instance this method extends.</returns>
        public static NotificationsBuilder AddEntityFrameworkRepositories<TContext>(this NotificationsBuilder builder)
            where TContext : NotificationDbContext
        {
            return builder.AddEntityFrameworkRepositories<TContext, NotificationsRepository<TContext>, NotificationRulesRepository<TContext>>();
        }

        /// <summary>
        /// Adds an Entity Framework implementation of notifications information repositories.
        /// </summary>
        /// <typeparam name="TContext">The Entity Framework database context to use.</typeparam>
        /// <typeparam name="TNotificationsRepository">The type representing a notifications repository in the system.</typeparam>
        /// <typeparam name="TRulesRepository">The type representing a rules repository in the system.</typeparam>
        /// <param name="builder">The <see cref="NotificationsBuilder"/> instance this method extends.</param>
        /// <returns>The <see cref="NotificationsBuilder"/> instance this method extends.</returns>
        public static NotificationsBuilder AddEntityFrameworkRepositories<TContext, TNotificationsRepository, TRulesRepository>(this NotificationsBuilder builder)
            where TContext : NotificationDbContext
            where TNotificationsRepository : class, INotificationsRepository
            where TRulesRepository : class, INotificationRulesRepository
        {
            builder.Services.TryAddScoped<INotificationsRepository, TNotificationsRepository>();
            builder.Services.TryAddScoped<INotificationRulesRepository, TRulesRepository>();

            return builder;
        }
    }
}