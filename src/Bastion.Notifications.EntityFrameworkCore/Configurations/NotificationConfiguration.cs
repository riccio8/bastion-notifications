﻿using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationConfiguration : IEntityTypeConfiguration<Notification>
    {
        public void Configure(EntityTypeBuilder<Notification> builder)
        {
            builder.ToTable("notification", "notification");

            builder.HasKey(e => e.NotificationId)
                .HasName("notification_pkey");

            builder.Property(e => e.NotificationId)
                .HasColumnName("notification_id")
                .HasMaxLength(15)
                .ValueGeneratedNever();

            builder.Property(e => e.Body)
                .IsRequired()
                .HasColumnName("body");

            builder.OwnsOne(e => e.FromUser,
                sa =>
                {
                    sa.Property(p => p.UserId).HasColumnName("from_user_id").HasMaxLength(36);
                    sa.Property(p => p.UserName).HasColumnName("from_user_name").HasMaxLength(255);
                });

            builder.OwnsOne(e => e.ToUser,
                sa =>
                {
                    sa.Property(p => p.UserId).HasColumnName("to_user_id").HasMaxLength(36);
                    sa.Property(p => p.UserName).HasColumnName("to_user_name").HasMaxLength(255);
                });

            builder.Property(e => e.NotificationAnswerId)
                .HasColumnName("notification_answer_id")
                .HasDefaultValue('-');

            builder.Property(e => e.PreviousAnswerId)
                .HasColumnName("previous_notification_answer_id")
                .HasDefaultValue('-');

            builder.Property(e => e.AnswerNotificationId)
                .HasColumnName("answer_notification_id")
                .HasMaxLength(15);

            builder.Property(e => e.PreviousNotificationId)
                .HasColumnName("previous_notification_id")
                .HasMaxLength(15);

            builder.Property(e => e.IsRead)
                .HasColumnName("read");

            builder.Property(e => e.SentAt)
                .HasColumnName("sent_ts")
                .ValueGeneratedOnAdd();

            builder.Property(e => e.AnswerAt)
                .HasColumnName("answer_ts");

            builder.Property(e => e.Subject)
                .IsRequired()
                .HasColumnName("subject")
                .HasMaxLength(255);

            builder.Property(e => e.Type)
                .HasColumnName("type")
                .HasMaxLength(255);

            builder.HasOne(d => d.NotificationAnswer)
                .WithMany(p => p.NotificationAnswers)
                .HasForeignKey(d => d.NotificationAnswerId)
                .OnDelete(DeleteBehavior.ClientSetNull)
                .HasConstraintName("notification_notification_answer_id_fkey");

            builder.HasOne(d => d.NotificationPreviousAnswer)
                .WithMany(p => p.NotificationPreviousAnswers)
                .HasForeignKey(d => d.PreviousAnswerId)
                .HasConstraintName("notification_previous_notification_answer_id_fkey");
        }
    }
}