﻿using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationAttachmentConfiguration : IEntityTypeConfiguration<NotificationAttachment>
    {
        public void Configure(EntityTypeBuilder<NotificationAttachment> builder)
        {
            builder.HasKey(e => new { e.NotificationAttachmentId, e.NotificationId })
                .HasName("notification_attachment_pkey");

            builder.ToTable("notification_attachment", "notification");

            builder.Property(e => e.NotificationAttachmentId)
                .HasColumnName("notification_attachment_id")
                .HasMaxLength(36)
                .ValueGeneratedNever();

            builder.Property(e => e.NotificationId)
                .IsRequired()
                .HasColumnName("notification_id")
                .HasMaxLength(15);

            builder.HasOne(d => d.NotificationFile)
                .WithOne(p => p.NotificationAttachment)
                .HasForeignKey<NotificationAttachment>(d => d.NotificationAttachmentId)
                .HasConstraintName("notification_attachment_notification_file_fkey");

            builder.HasOne(d => d.Notification)
                .WithMany(p => p.Attachments)
                .HasForeignKey(d => d.NotificationId)
                .HasConstraintName("notification_attachment_notification_id_fkey");
        }
    }
}