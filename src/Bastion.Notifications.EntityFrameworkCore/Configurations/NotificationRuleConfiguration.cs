﻿using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationRuleConfiguration : IEntityTypeConfiguration<NotificationRule>
    {
        public void Configure(EntityTypeBuilder<NotificationRule> builder)
        {
            builder.HasKey(e => e.NotificationRuleId)
                .HasName("notification_rule_pkey");

            builder.ToTable("notification_rule", "notification");

            builder.Property(e => e.NotificationRuleId)
                .HasColumnName("rule_id")
                .HasMaxLength(15)
                .ValueGeneratedNever();

            builder.Property(e => e.Name)
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.Property(e => e.TemplateIdApproved)
                .HasColumnName("template_id_approved")
                .HasMaxLength(255);

            builder.Property(e => e.TemplateIdPending)
                .HasColumnName("template_id_pending")
                .HasMaxLength(255);

            builder.Property(e => e.TemplateIdRejected)
                .HasColumnName("template_id_rejected")
                .HasMaxLength(255);
        }
    }
}