﻿using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationFileConfiguration : IEntityTypeConfiguration<NotificationFile>
    {
        public void Configure(EntityTypeBuilder<NotificationFile> builder)
        {
            builder.HasKey(e => e.NotificationAttachmentId)
                .HasName("notification_file_pkey");

            builder.ToTable("notification_file", "notification");

            builder.Property(e => e.NotificationAttachmentId)
                .HasColumnName("notification_attachment_id")
                .HasMaxLength(36);

            builder.Property(p => p.UserId)
                .HasColumnName("user_id")
                .IsRequired()
                .HasMaxLength(36)
                .ValueGeneratedNever();

            builder.Property(e => e.FileData)
                .IsRequired()
                .HasColumnName("file_data");

            builder.Property(e => e.FileExtension)
                .HasColumnName("file_extension")
                .HasMaxLength(255);

            builder.Property(e => e.ContentType)
                .HasColumnName("content_type")
                .HasMaxLength(255);

            builder.Property(e => e.FileUploadAt)
                .HasColumnName("file_upload_ts")
                .HasDefaultValueSql("now()");
        }
    }
}