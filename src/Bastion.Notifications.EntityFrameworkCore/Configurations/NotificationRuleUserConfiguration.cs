﻿using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationRuleUserConfiguration : IEntityTypeConfiguration<NotificationRuleUser>
    {
        public void Configure(EntityTypeBuilder<NotificationRuleUser> builder)
        {
            builder.HasKey(e => e.NotificationRuleId)
                .HasName("notification_rule_item_pkey");

            builder.ToTable("notification_rule_user", "notification");

            builder.Property(e => e.NotificationRuleId)
                .HasColumnName("notification_rule_id")
                .HasMaxLength(15)
                .ValueGeneratedNever();

            builder.OwnsOne(e => e.User,
                sa =>
                {
                    sa.Property(p => p.UserId).HasColumnName("user_id").IsRequired().HasMaxLength(36).ValueGeneratedNever();
                    sa.Property(p => p.UserName).HasColumnName("user_name").HasMaxLength(255);
                });

            builder.HasOne(d => d.NotificationRule)
                .WithMany(p => p.Users)
                .HasForeignKey(d => d.NotificationRuleId)
                .HasConstraintName("notification_rule_user_notification_rule_id_fkey");
        }
    }
}