﻿using Bastion.Notifications.Constants;
using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace Bastion.Notifications.EntityFrameworkCore.Configurations
{
    internal class NotificationAnswerConfiguration : IEntityTypeConfiguration<NotificationAnswer>
    {
        public void Configure(EntityTypeBuilder<NotificationAnswer> builder)
        {
            builder.ToTable("notification_answer", "notification");

            builder.Property(e => e.NotificationAnswerId)
                .HasColumnName("notification_answer_id")
                .ValueGeneratedNever();

            builder.Property(e => e.Name)
                .IsRequired()
                .HasColumnName("name")
                .HasMaxLength(255);

            builder.HasData(GetSeedData());
        }

        private NotificationAnswer[] GetSeedData()
        {
            return new NotificationAnswer[]
            {
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.NoAnswer, Name = "No answer" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Approve, Name = "Approve" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Reject, Name = "Reject" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Forward, Name = "Forward" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.More, Name = "Approve and Forward (More approval needed)" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Pending, Name = "Pending complaince" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Processing, Name = "Processing" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.Completed, Name = "Completed" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.PendingWithdraw, Name = "Pending withdraw" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.MoneyInOurBank, Name = "Money in our bank" },
                new NotificationAnswer { NotificationAnswerId = NotificationAnswerConstants.MoneyTraveling, Name = "Money traveling to our account" },
            };
        }
    }
}