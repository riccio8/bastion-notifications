﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Stores;

using Microsoft.EntityFrameworkCore;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Bastion.Notifications.EntityFrameworkCore
{
    /// <summary>
    /// Notification rules repository.
    /// </summary>
    internal class NotificationRulesRepository<TContext> : INotificationRulesRepository where TContext : NotificationDbContext
    {
        private readonly TContext notificationContext;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationRulesRepository"/> class.
        /// </summary>
        public NotificationRulesRepository(TContext notificationContext)
        {
            this.notificationContext = notificationContext;
        }

        /// <summary>
        /// A navigation property for the rules the store contains.
        /// </summary>
        public IQueryable<NotificationRule> Rules => notificationContext.Rules;

        /// <summary>
        /// A navigation property for the rule users the store contains.
        /// </summary>
        public IQueryable<NotificationRuleUser> RuleUsers => notificationContext.RuleUsers;

        /// <summary>
        /// Get users for rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <returns>The task of list of <see cref="NotificationRuleUser"/>.</returns>
        public Task<List<NotificationRuleUser>> GetUsersForRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.RuleUsers
                .AsNoTracking()
                .Where(x => x.NotificationRuleId == ruleId)
                .ToListAsync(cancellationToken);
        }

        /// <summary>
        /// Add user for rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <param name="userId">User id.</param>
        /// <param name="userName">User name.</param>
        /// <returns>The task of <see cref="NotificationRuleUser"/>.</returns>
        public async Task<NotificationRuleUser> AddUserForRuleAsync(
            string ruleId,
            string userId,
            string userName,
            CancellationToken cancellationToken = default)
        {
            var ruleUser = new NotificationRuleUser
            {
                NotificationRuleId = ruleId,
                User = new NotificationUser
                {
                    UserId = userId,
                    UserName = userName,
                }
            };

            notificationContext.RuleUsers.Add(ruleUser);
            await SaveChangesAsync(cancellationToken);

            return ruleUser;
        }

        /// <summary>
        /// Delete user from rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <param name="userId">User id.</param>
        /// <returns>The task of <see cref="NotificationRuleUser"/>.</returns>
        public async Task<NotificationRuleUser> DeleteUserFromRuleAsync(
            string ruleId,
            string userId,
            CancellationToken cancellationToken = default)
        {
            var ruleUser = await notificationContext.RuleUsers
                .FirstOrDefaultAsync(rule => rule.NotificationRuleId == ruleId && rule.User.UserId == userId);
            if (ruleUser is null)
            {
                return null;
            }

            notificationContext.RuleUsers.Remove(ruleUser);
            await SaveChangesAsync(cancellationToken);

            return ruleUser;
        }

        /// <summary>
        /// Get notification rules.
        /// </summary>
        /// <returns>The Task of list of <see cref="NotificationRule"/> rules.</returns>
        public async Task<List<NotificationRule>> GetRulesAsync(
            CancellationToken cancellationToken = default)
        {
            return await notificationContext.Rules
                .Include(x => x.Users)
                .AsNoTracking()
                .ToListAsync(cancellationToken);
        }

        /// <summary>
        /// Get notification rule by id.
        /// </summary>
        /// <param name="ruleId">Rule Id.</param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        public Task<NotificationRule> GetRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default)
        {
            return notificationContext.Rules
                .Include(x => x.Users)
                .AsNoTracking()
                .FirstOrDefaultAsync(rule => rule.NotificationRuleId == ruleId, cancellationToken);
        }

        /// <summary>
        /// Add notification rule.
        /// </summary>
        /// <param name="notificationRule"></param>
        /// <param name="cancellationToken"></param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        public async Task<NotificationRule> AddRuleAsync(
            NotificationRule notificationRule,
            CancellationToken cancellationToken = default)
        {
            if (notificationRule is null)
            {
                throw new ArgumentNullException(nameof(notificationRule));
            }

            await notificationContext.Rules.AddAsync(notificationRule, cancellationToken);
            await SaveChangesAsync(cancellationToken);

            return notificationRule;
        }

        /// <summary>
        /// Delete notification rule.
        /// </summary>
        /// <param name="notificationRule"></param>
        /// <returns>The task of <see cref="NotificationRule"/> rule.</returns>
        public async Task<NotificationRule> DeleteRuleAsync(
            string ruleId,
            CancellationToken cancellationToken = default)
        {
            var notificationRule = await notificationContext.Rules
                .FirstOrDefaultAsync(rule => rule.NotificationRuleId == ruleId, cancellationToken);
            if (notificationRule is null)
            {
                return null;
            }

            notificationContext.Rules.Remove(notificationRule);
            await SaveChangesAsync(cancellationToken);

            return notificationRule;
        }

        private protected Task<int> SaveChangesAsync(CancellationToken cancellationToken)
        {
            return notificationContext.SaveChangesAsync(cancellationToken);
        }
    }
}