﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Api.Models;
using Bastion.Notifications.Api.Models.Extensions;
using Bastion.Notifications.Api.Models.Requests;
using Bastion.Notifications.Api.Models.Responses;
using Bastion.Notifications.Constants;
using Bastion.Notifications.Exceptions;
using Bastion.Notifications.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.StaticFiles;

using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mime;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Bastion.Notifications.Controllers
{
    /// <summary>
    /// Notifications controller.
    /// </summary>
    [Authorize]
    [ApiController]
    [Route("api/v1/notifications")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    public class NotificationsController : ControllerBase
    {
        private readonly INotificationsService notificationService;
        private readonly FileExtensionContentTypeProvider provider;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsController"/> class.
        /// </summary>
        public NotificationsController(
            INotificationsService notificationService)
        {
            this.notificationService = notificationService;
            provider = new FileExtensionContentTypeProvider();
        }

        /// <summary>
        /// Get notifications.
        /// </summary>
        [HttpGet]
        public async Task<NotificationPageResponse> GetNotificationsAsync(
            [FromQuery] NotificationsFilterRequest filterRequest,
            [FromQuery] PaginationRequest paginationRequest)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
              ?? throw new UserIdentifierNotFoundException();

            var notifications = await notificationService.GetNotificationsAsync(
                toUserId: paginationRequest.ToUserByDirection(NotificationDirection.Incoming, userId),
                fromUserId: paginationRequest.ToUserByDirection(NotificationDirection.Outgoing, userId),
                isRead: filterRequest.ToIsRead(),
                answers: filterRequest.ToAnswers(),
                type: filterRequest.Type?.Select(x => x.ToUpperInvariant()).FirstOrDefault(),
                search: filterRequest.Search?.ToUpperInvariant(),
                sortByDirection: paginationRequest.GetSortByDirection(),
                pageIndex: paginationRequest.Page - 1,
                pageSize: paginationRequest.PerPage);

            return new NotificationPageResponse
            {
                Size = notifications.PageSize,
                Page = paginationRequest.Page,
                Count = notifications.CountTotal,
                CountPages = notifications.PageCount,
                Notifications = notifications.Select(x => x.ToShortResponse(userId))
            };
        }

        /// <summary>
        /// Get notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        [HttpGet("{notificationId}")]
        public async Task<NotificationDetailResponse> GetNotificationAsync(string notificationId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var notification = await notificationService.GetNotificationAsync(
                userId,
                notificationId)
                ?? throw new NotificationNotFoundException(notificationId);

            if (userId == notification.ToUser.UserId)
            {
                notification = await notificationService.SetNotificationAsReadAsync(notification);
            }

            return notification.ToDetailResponse(userId);
        }

        /// <summary>
        /// Delete notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        [HttpDelete("{notificationId}")]
        public async Task<NotificationDetailResponse> DeleteNotificationAsync(string notificationId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var notification = await notificationService.GetNotificationAsync(
                userId,
                notificationId)
                ?? throw new NotificationNotFoundException(notificationId);

            if (userId == notification.ToUser.UserId || userId == notification.FromUser.UserId)
            {
                notification = await notificationService.DeleteAsync(notification);
            }

            return notification.ToDetailResponse(userId);
        }

        /// <summary>
        /// Add notification by rule.
        /// </summary>
        /// <param name="ruleId">Rule id.</param>
        /// <param name="request"></param>
        [HttpPost("byRule/{ruleId}")]
        public async Task<IEnumerable<NotificationIdResponse>> AddNotificationByRuleAsync(string ruleId, NotificationRequest request)
        {
            var answerId = AnswerToAnswerId(request.Answer)
                ?? throw new NotificationAnswerUnknownException();

            var userIdFrom = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var userNameFrom = User.FindFirst(ClaimTypes.Name)?.Value ?? default;

            var notifications = await notificationService.AddByRuleAsync(
                new NotificationUser
                {
                    UserId = userIdFrom,
                    UserName = userNameFrom,
                },
                ruleId?.ToUpper(),
                request.Subject,
                request.Body,
                answerId,
                attachments: request?.Attachments?.Select(x => x.Id));

            return notifications.Select(x => x.ToIdResponse());
        }

        /// <summary>
        /// Add notification to user.
        /// </summary>
        /// <param name="userIdTo">To user id.</param>
        /// <param name="request">Request.</param>
        [HttpPost("toUser/{userIdTo}")]
        public async Task<NotificationIdResponse> AddNotificationToUserAsync(string userIdTo, NotificationRequest request)
        {
            var answerId = AnswerToAnswerId(request.Answer)
                ?? throw new NotificationAnswerUnknownException();

            var userIdFrom = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var userNameFrom = User.FindFirst(ClaimTypes.Name)?.Value ?? default;

            var notification = await notificationService.AddAsync(
                userFrom: new NotificationUser
                {
                    UserId = userIdFrom,
                    UserName = userNameFrom,
                },
                userTo: new NotificationUser
                {
                    UserId = userIdTo,
                    UserName = request.UserNameForwardTo,
                },
                request.Type,
                request.Subject,
                request.Body,
                answerId,
                attachments: request?.Attachments?.Select(x => x.Id));

            return notification.ToIdResponse();
        }

        /// <summary>
        /// Add notification to user.
        /// </summary>
        [HttpPost("all/markAsRead")]
        public async Task<IEnumerable<NotificationIdResponse>> MarkAsReadAllAsync()
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var notifications = await notificationService.SetNotificationsAsReadAsync(userId);

            return notifications.Select(x => x.ToIdResponse());
        }

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userIdForwardTo">To user id.</param>
        /// <param name="answer">Notification answer.</param>
        /// <param name="request"></param>
        [HttpPost("{notificationId}/{answer}/answer/{userIdTo?}")]
        public async Task<NotificationIdResponse> AnswerNotificationAsync(
            string notificationId,
            string answer,
            NotificationRequest request,
            string userIdForwardTo = default)
        {
            var answerId = AnswerToAnswerId(answer)
                ?? throw new NotificationAnswerUnknownException();

            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();
            var userName = User.FindFirst(ClaimTypes.Name)?.Value ?? default;

            var notification = await notificationService.AddAnswerAsync(
                notificationId: notificationId,
                answerId: answerId,
                body: request.Body,
                subject: request.Subject,
                attachments: request?.Attachments?.Select(x => x.Id),
                userFrom: new NotificationUser { UserId = userId, UserName = userName },
                userForwardTo: string.IsNullOrEmpty(userIdForwardTo) ? default : new NotificationUser { UserId = userIdForwardTo, UserName = request.UserNameForwardTo });

            return notification.ToIdResponse();
        }

        private static char? AnswerToAnswerId(string answer)
        {
            switch (answer?.ToUpperInvariant())
            {
                case "REJECT":
                case "R":
                    return NotificationAnswerConstants.Reject;

                case "APPROVE":
                case "A":
                    return NotificationAnswerConstants.Approve;

                case "FORWARD":
                case "F":
                    return NotificationAnswerConstants.Forward;

                case "MORE":
                case "M":
                    return NotificationAnswerConstants.More;

                case "NOANSWER":
                case "-":
                    return NotificationAnswerConstants.NoAnswer;

                case "PENDING":
                case "P":
                    return NotificationAnswerConstants.Pending;

                case "COMPLETED":
                case "C":
                    return NotificationAnswerConstants.Completed;

                case "PROCESSING":
                case "S":
                    return NotificationAnswerConstants.Processing;

                case "PENDINGWITHDRAW":
                case "W":
                    return NotificationAnswerConstants.PendingWithdraw;

                case "MONEYINOURBANK":
                case "B":
                    return NotificationAnswerConstants.MoneyInOurBank;

                case "MONEYTRAVELING":
                case "T":
                    return NotificationAnswerConstants.MoneyTraveling;

                default:
                    return default;
            }
        }

        /// <summary>
        /// Upload file.
        /// </summary>
        [HttpPost("upload")]
        [Consumes("multipart/form-data")]
        public async Task<NotificationIdResponse> UploadFileAsync(IFormFile file)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            using var stream = new MemoryStream();
            await file.CopyToAsync(stream).ConfigureAwait(false);

            var notificationFile = await notificationService.AddFileAsync(
                new NotificationFile
                {
                    UserId = userId,
                    FileData = stream.ToArray(),
                    FileUploadAt = DateTime.UtcNow,
                    ContentType = file.ContentType,
                    FileExtension = Path.GetExtension(file.FileName),
                    NotificationAttachmentId = Guid.NewGuid().ToString(),
                });
            return new NotificationIdResponse { Id = notificationFile.NotificationAttachmentId };
        }

        /// <summary>
        /// Download file by attachment id.
        /// </summary>
        [HttpGet("{notificationId}/files/{attachmentId}")]
        public async Task<FileStreamResult> DownloadFileAsync(string notificationId, string attachmentId)
        {
            var userId = User.FindFirst(ClaimTypes.NameIdentifier)?.Value
                ?? throw new UserIdentifierNotFoundException();

            var notification = await notificationService.GetNotificationAsync(
               userId,
               notificationId)
               ?? throw new NotificationNotFoundException(notificationId);

            var attachment = notification.Attachments.FirstOrDefault(x => x.NotificationAttachmentId == attachmentId)
                ?? throw new AttachmentNotFoundException(notification.NotificationId, attachmentId);

            var file = await notificationService.GetFileAsync(attachmentId)
                ?? throw new AttachmentNotFoundException(notification.NotificationId, attachment.NotificationAttachmentId);

            var contentType = file.ContentType;
            if (string.IsNullOrEmpty(contentType) && !provider.TryGetContentType(file.FileExtension, out contentType))
            {
                contentType = "application/octet-stream";
            }

            return new FileStreamResult(new MemoryStream(file.FileData), contentType);
        }
    }
}