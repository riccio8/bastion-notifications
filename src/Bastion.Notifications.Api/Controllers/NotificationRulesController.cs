﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Api.Models.Extensions;
using Bastion.Notifications.Api.Models.Requests;
using Bastion.Notifications.Api.Models.Responses;
using Bastion.Notifications.Constants;
using Bastion.Notifications.Exceptions;
using Bastion.Notifications.Stores;

using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

using System.Collections.Generic;
using System.Linq;
using System.Net.Mime;
using System.Threading.Tasks;

namespace Bastion.Notifications.Api.Controllers
{
    /// <summary>
    /// Notification rules controller.
    /// </summary>
    [ApiController]
    [Route("api/v1/notifications/rules")]
    [Consumes(MediaTypeNames.Application.Json)]
    [Produces(MediaTypeNames.Application.Json)]
    [Authorize(Policy = NotificationPolicyConstants.ManagementPolicy)]
    public class NotificationRulesController : ControllerBase
    {
        private readonly INotificationRulesRepository rulesRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationRulesController"/> class.
        /// </summary>
        public NotificationRulesController(INotificationRulesRepository rulesRepository)
        {
            this.rulesRepository = rulesRepository;
        }

        /// <summary>
        /// Get notification rules.
        /// </summary>
        [HttpGet]
        public async Task<IEnumerable<NotificationRuleResponse>> GetRulesAsync()
        {
            var rules = await rulesRepository.GetRulesAsync();

            return rules.Select(x => x.ToResponse());
        }

        /// <summary>
        /// Get notification rule by id.
        /// </summary>
        /// <param name="ruleId">Notification rule id.</param>
        [HttpGet("{ruleId}")]
        public async Task<NotificationRuleResponse> GetRuleAsync(string ruleId)
        {
            var rule = await rulesRepository.GetRuleAsync(ruleId)
                ?? throw new NotificationRuleNotFoundException(ruleId);

            return rule.ToResponse();
        }

        /// <summary>
        /// Add notification rule.
        /// </summary>
        [HttpPost]
        public async Task<NotificationRuleResponse> AddRuleAsync(NotificationRuleRequest request)
        {
            var rule = await rulesRepository.AddRuleAsync(new NotificationRule
            {
                Name = request.Name,
                NotificationRuleId = request.RuleId,
                TemplateIdPending = request.TemplateIdPending,
                TemplateIdApproved = request.TemplateIdApproved,
                TemplateIdRejected = request.TemplateIdRejected,
            });

            return rule.ToResponse();
        }

        /// <summary>
        /// Delete notification rule by id.
        /// </summary>
        /// <param name="ruleId">Notification rule id.</param>
        [HttpDelete("{ruleId}")]
        public async Task<NotificationRuleResponse> DeleteRuleAsync(string ruleId)
        {
            var rule = await rulesRepository.DeleteRuleAsync(ruleId)
                ?? throw new NotificationRuleNotFoundException(ruleId);

            return rule.ToResponse();
        }

        /// <summary>
        /// Get user for notification rule by id.
        /// </summary>
        /// <param name="ruleId">Notification rule id.</param>
        [HttpGet("{ruleId}/users")]
        public async Task<IEnumerable<NotificationRuleUserResponse>> GetUsersForRuleAsync(string ruleId)
        {
            var users = await rulesRepository.GetUsersForRuleAsync(ruleId);

            return users.Select(x => x.ToResponse());
        }

        /// <summary>
        /// Add user for notification rule.
        /// </summary>
        /// <param name="ruleId">Notification rule id.</param>
        /// <param name="request">Request.</param>
        [HttpPost("{ruleId}/users")]
        public async Task<NotificationRuleUserResponse> DeleteUserFromRuleAsync(string ruleId, NotificationRuleUserRequest request)
        {
            var user = await rulesRepository.AddUserForRuleAsync(ruleId, request.UserId, request.UserName);

            return user.ToResponse();
        }

        /// <summary>
        /// Delete user from notification rule.
        /// </summary>
        /// <param name="ruleId">Notification rule id.</param>
        /// <param name="userId">User id.</param>
        [HttpDelete("{ruleId}/users/{userid}")]
        public async Task<NotificationRuleUserResponse> DeleteUserFromRuleAsync(string ruleId, string userId)
        {
            var user = await rulesRepository.DeleteUserFromRuleAsync(ruleId, userId)
                ?? throw new NotificationUserForRuleNotFoundException(ruleId, userId);

            return user.ToResponse();
        }
    }
}