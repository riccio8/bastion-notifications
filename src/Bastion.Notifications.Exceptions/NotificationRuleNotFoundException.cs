﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification rule not found exception
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationRuleNotFoundException : NotificationExceptionBase
    {
        public NotificationRuleNotFoundException(string ruleId) : base($"Rule '{ruleId}' not found.")
        {
        }
    }
}