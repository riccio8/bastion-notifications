﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification users for rule not found exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationUsersForRuleNotFoundException : NotificationExceptionBase
    {
        public NotificationUsersForRuleNotFoundException(string ruleId) : base($"Users for rule: '{ruleId}', not found.")
        {
        }
    }
}