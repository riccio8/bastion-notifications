﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Attachment not found exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class AttachmentNotFoundException : NotificationExceptionBase
    {
        public AttachmentNotFoundException(string notificationId, string attachmentId)
            : base($"Attachment '{attachmentId}' for notification '{notificationId}', not found.")
        {
        }
    }
}