﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification user for rule not found exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationUserForRuleNotFoundException : NotificationExceptionBase
    {
        public NotificationUserForRuleNotFoundException(string ruleId, string userId) : base($"User '{userId}' for rule: '{ruleId}' not found.")
        {
        }
    }
}