﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification repeated answer exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationRepeatedAnswerException : NotificationExceptionBase
    {
        public NotificationRepeatedAnswerException(string notificationId, string notificationAnswer, string newAnswer)
            : base($"Notification {notificationId} repeated answer '{notificationAnswer}', attempt to assign status: '{newAnswer}'.")
        {
        }
    }
}