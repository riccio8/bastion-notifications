﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification not found exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationNotFoundException : NotificationExceptionBase
    {
        public NotificationNotFoundException(string notificationId) : base($"Notification '{notificationId}', not found.")
        {
        }
    }
}