﻿using System;
using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// User identifier not found exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class UserIdentifierNotFoundException : Exception
    {
        public UserIdentifierNotFoundException() : base("User identifier, not found.")
        {
        }
    }
}