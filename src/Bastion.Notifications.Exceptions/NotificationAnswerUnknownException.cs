﻿using System.Diagnostics.CodeAnalysis;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification answer unknown exception.
    /// </summary>
    [SuppressMessage("Design", "CA1032:Implement standard exception constructors")]
    public class NotificationAnswerUnknownException : NotificationExceptionBase
    {
        public NotificationAnswerUnknownException() : base("Answer of type unknown.")
        {
        }
    }
}