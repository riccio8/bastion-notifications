﻿using System;

namespace Bastion.Notifications.Exceptions
{
    /// <summary>
    /// Notification exception.
    /// </summary>
    public abstract class NotificationExceptionBase : Exception
    {
        protected NotificationExceptionBase(string message) : base(message)
        {
        }

        protected NotificationExceptionBase(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected NotificationExceptionBase()
        {
        }
    }
}