﻿using Bastion.Notifications.Extensions;
using Bastion.Notifications.Options;

using System;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="NotificationsBuilder"/>.
    /// </summary>
    public static class NotificationsBuilderExtensions
    {
        /// <summary>
        /// Adds a configuration implementation.
        /// </summary>
        /// <param name="builder">The <see cref="NotificationsBuilder"/> instance this method extends.</param>
        /// <param name="setupAction">An action to configure the <see cref="NotificationsOptions"/>.</param>
        /// <returns>The <see cref="NotificationsBuilder"/> instance this method extends.</returns>
        public static NotificationsBuilder AddConfiguration(this NotificationsBuilder builder,
            Action<NotificationsOptions> setupAction)
        {
            if (setupAction != null)
            {
                builder.Services.Configure(setupAction);
            }
            return builder;
        }
    }
}