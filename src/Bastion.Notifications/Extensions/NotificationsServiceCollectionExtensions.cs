﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Extensions;
using Bastion.Notifications.Hubs;
using Bastion.Notifications.Services;

using Microsoft.Extensions.DependencyInjection.Extensions;

namespace Microsoft.Extensions.DependencyInjection
{
    /// <summary>
    /// Contains extension methods to <see cref="IServiceCollection"/> for configuring notifications services.
    /// </summary>
    public static class NotificationsServiceCollectionExtensions
    {
        /// <summary>
        /// Adds default the notifications system.
        /// </summary>
        /// <param name="services">The services available in the application.</param>
        /// <returns>An <see cref="NotificationsBuilder"/> for creating and configuring the notifications system.</returns>
        public static NotificationsBuilder AddNotifications(this IServiceCollection services)
        {
            return services.AddNotifications<NotificationsService<NotificationsHub>, NotificationsHub>();
        }

        /// <summary>
        /// Adds and configures the notifications system for the specified service and hub types.
        /// </summary>
        /// <typeparam name="TNotificationsService">The type representing a notifications service in the system.</typeparam>
        /// <typeparam name="TNotificationsHub">The type representing a notifications signalr hub in the system.</typeparam>
        /// <param name="services">The services available in the application.</param>
        /// <returns>An <see cref="NotificationsBuilder"/> for creating and configuring the notifications system.</returns>
        public static NotificationsBuilder AddNotifications<TNotificationsService, TNotificationsHub>(
            this IServiceCollection services)
            where TNotificationsHub : NotificationsHub
            where TNotificationsService : NotificationsService<TNotificationsHub>
        {
            services.TryAddScoped<INotificationsService, TNotificationsService>();

            return new NotificationsBuilder(services);
        }
    }
}