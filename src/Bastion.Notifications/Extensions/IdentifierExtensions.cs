﻿using System;

namespace Bastion.Notifications.Extensions
{
    /// <summary>
    /// Identifier helper.
    /// </summary>
    public static class IdentifierExtensions
    {
        /// <summary>
        /// Generate identifier.
        /// </summary>
        /// <param name="length">Length.</param>
        /// <returns>Identifier.</returns>
        public static string GenerateIdentifier(int length)
        {
            if (length < 1)
            {
                throw new ArgumentOutOfRangeException(nameof(length));
            }

            var identifier = string.Empty;
            do
            {
                var id = Convert.ToBase64String(Guid.NewGuid().ToByteArray())
                    .Replace("/", string.Empty)
                    .Replace("+", string.Empty)
                    .Replace("=", string.Empty);

                var left = length - identifier.Length;
                identifier += left > id.Length ? id : id.Substring(0, left);
            }
            while (length > identifier.Length);

            return identifier.ToUpperInvariant();
        }
    }
}