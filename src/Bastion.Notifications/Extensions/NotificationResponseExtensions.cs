﻿using Bastion.Notifications.Api.Models.Extensions;
using Bastion.Notifications.Api.Models.Responses;
using Bastion.Notifications.Models.Responses;
using Bastion.Notifications.Stores;

using System.Collections.Generic;
using System.Linq;

namespace Bastion.Notifications.Extensions
{
    /// <summary>
    /// Notification response extensions.
    /// </summary>
    public static class NotificationResponseExtensions
    {
        /// <summary>
        /// To read response.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="countUnread">Count unread.</param>
        public static NotificationsReadResponse ToReadResponse(this Notification notification, int countUnread)
        {
            return new NotificationsReadResponse
            {
                CountUnread = countUnread,
                Read = new NotificationIdResponse[] { notification.ToIdResponse() }
            };
        }

        /// <summary>
        /// To read response.
        /// </summary>
        /// <param name="notification">Notifications.</param>
        /// <param name="countUnread">Count unread.</param>
        public static NotificationsReadResponse ToReadResponse(this IEnumerable<Notification> notifications, int countUnread)
        {
            return new NotificationsReadResponse
            {
                CountUnread = countUnread,
                Read = notifications.Select(x => x.ToIdResponse()).ToList()
            };
        }
    }
}