﻿using Bastion.Notifications.Abstractions;

namespace Bastion.Notifications.Models.Responses
{
    /// <summary>
    /// Notifications unread response.
    /// </summary>
    public class NotificationsUnreadResponse : INotifiable
    {
        /// <summary>
        /// Count unread.
        /// </summary>
        public int Count { get; set; }

        /// <summary>
        /// SignalR method.
        /// </summary>
        public string NotificationMethod => "onNotificationsUnread";
    }
}