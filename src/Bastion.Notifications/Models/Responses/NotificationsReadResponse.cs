﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Api.Models.Responses;

using System.Collections.Generic;

namespace Bastion.Notifications.Models.Responses
{
    /// <summary>
    /// Notifications read response.
    /// </summary>
    public class NotificationsReadResponse : INotifiable
    {
        /// <summary>
        /// Count unread.
        /// </summary>
        public int CountUnread { get; set; }

        /// <summary>
        /// Read items.
        /// </summary>
        public ICollection<NotificationIdResponse> Read { get; set; }

        /// <summary>
        /// SignalR method.
        /// </summary>
        public string NotificationMethod => "onNotificationsRead";
    }
}