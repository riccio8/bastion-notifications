﻿using Bastion.Notifications.Abstractions;
using Bastion.Notifications.Api.Models.Extensions;
using Bastion.Notifications.Constants;
using Bastion.Notifications.Exceptions;
using Bastion.Notifications.Extensions;
using Bastion.Notifications.Hubs;
using Bastion.Notifications.Hubs.Extensions;
using Bastion.Notifications.Models.Responses;
using Bastion.Notifications.Options;
using Bastion.Notifications.Stores;

using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Bastion.Notifications.Services
{
    /// <summary>
    /// Notifications service.
    /// </summary>
    public class NotificationsService<THub> : INotificationsService where THub : NotificationsHub
    {
        private readonly NotificationsOptions options;
        private readonly IHubContext<THub> notificationHub;
        private readonly ILogger<INotificationsService> logger;
        private readonly INotificationRulesRepository rulesRepository;
        private readonly INotificationsRepository notificationsRepository;

        /// <summary>
        /// Initializes a new instance of the <see cref="NotificationsService{THub}"/> class.
        /// </summary>
        public NotificationsService(
            IHubContext<THub> notificationHub,
            ILogger<INotificationsService> logger,
            IOptions<NotificationsOptions> options,
            INotificationRulesRepository rulesRepository,
            INotificationsRepository notificationsRepository)
        {
            this.logger = logger;
            this.options = options.Value;
            this.notificationHub = notificationHub;
            this.rulesRepository = rulesRepository;
            this.notificationsRepository = notificationsRepository;
        }

        /// <summary>
        /// Send to user the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="userId">The user Id.</param>
        /// <returns>The task.</returns>
        public Task SendToUserAsync<TMessage>(
            TMessage message,
            string userId)
            where TMessage : INotifiable
        {
            return notificationHub.SendToUserAsync(message, userId);
        }

        /// <summary>
        /// Send to group the <paramref name="message"/>.
        /// </summary>
        /// <typeparam name="TMessage">The message type.</typeparam>
        /// <param name="message">The message to publish.</param>
        /// <param name="group">The group.</param>
        /// <returns>The task.</returns>
        public Task SendToGroupAsync<TMessage>(
            TMessage message,
            string group)
            where TMessage : INotifiable
        {
            return notificationHub.SendToGroupAsync(message, group);
        }

        /// <summary>
        /// Send notifications unread count.
        /// </summary>
        /// <param name="userId">User id</param>
        /// <returns>The task.</returns>
        public async Task SendUnreadCountAsync(
            string userId)
        {
            var countUnread = await notificationsRepository.GetCountUnreadAsync(userId);

            await SendToUserAsync(new NotificationsUnreadResponse { Count = countUnread }, userId);
        }

        /// <summary>
        /// Send notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>The task.</returns>
        public async Task SendAsync(
            Notification notification)
        {
            await SendUnreadCountAsync(notification.ToUser.UserId);

            await SendToUserAsync(notification.ToShortResponse(notification.ToUser.UserId), notification.ToUser.UserId);
        }

        /// <summary>
        /// Set all notifications as read for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>List of modified notifications.</returns>
        public async Task<List<Notification>> SetNotificationsAsReadAsync(
            string userId)
        {
            var notifications = await notificationsRepository.MarkAsReadAsync(userId);
            if (notifications.Count > 0)
            {
                var countUnread = await notificationsRepository.GetCountUnreadAsync(userId);
                await SendToUserAsync(notifications.ToReadResponse(countUnread), userId);
                await SendToUserAsync(new NotificationsUnreadResponse { Count = countUnread }, userId);
            }
            return notifications;
        }

        /// <summary>
        /// Set notification as read.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>Modified notification.</returns>
        public async Task<Notification> SetNotificationAsReadAsync(
            Notification notification)
        {
            if (!notification.IsRead)
            {
                notification = await notificationsRepository.SetNotificationAsReadAsync(notification);

                if (notification.IsRead)
                {
                    var countUnread = await notificationsRepository.GetCountUnreadAsync(notification.ToUser.UserId);
                    await SendToUserAsync(notification.ToReadResponse(countUnread), notification.ToUser.UserId);
                    await SendToUserAsync(new NotificationsUnreadResponse { Count = countUnread }, notification.ToUser.UserId);
                }
            }
            return notification;
        }

        /// <summary>
        /// Change notification subject.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="subject">Notification subject.</param>
        /// <returns>Modified notification.</returns>
        public Task<Notification> ChangeNotificationSubjectAsync(Notification notification, string subject)
        {
            return notificationsRepository.ChangeNotificationSubjectAsync(notification, subject);
        }

        /// <summary>
        /// Delete notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <returns>Deleted notification.</returns>
        public async Task<Notification> DeleteAsync(
            Notification notification)
        {
            var deletedNotification = await notificationsRepository.DeleteAsync(notification);
            if (deletedNotification != null)
            {
                await SendUnreadCountAsync(deletedNotification.ToUser.UserId);
            }

            return deletedNotification;
        }

        /// <summary>
        /// Get all notifications for user.
        /// </summary>
        /// <param name="userId">User id.</param>
        /// <returns>The task list of <see cref="Notification"/>.</returns>
        public Task<List<Notification>> GetNotificationsAsync(
            string userId)
        {
            return notificationsRepository.GetAsync(userId);
        }

        /// <summary>
        /// Get notifications for user.
        /// </summary>
        /// <param name="fromUserId">From user id.</param>
        /// <param name="toUserId">To user id.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetNotificationsAsync(
            string fromUserId,
            string toUserId,
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize)
        {
            return notificationsRepository.GetPaginatedAsync(
                fromUserId: fromUserId,
                toUserId: toUserId,
                search: search,
                isRead: isRead,
                answers: answers,
                type: type,
                sortByDirection: sortByDirection,
                pageIndex: pageIndex,
                pageSize: pageSize);
        }

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="type">Type notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetNotificationsAsync(
            string search,
            bool? isRead,
            string answers,
            string type,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default)
        {
            return notificationsRepository.GetPaginatedAsync(
                fromUserIds: fromUserIds,
                toUserIds: toUserIds,
                search: search,
                isRead: isRead,
                answers: answers,
                type: type,
                sortByDirection: sortByDirection,
                pageIndex: pageIndex,
                pageSize: pageSize);
        }

        /// <summary>
        /// Get notifications for users.
        /// </summary>
        /// <param name="fromUserIds">From user ids.</param>
        /// <param name="toUserIds">To user ids.</param>
        /// <param name="search">Search.</param>
        /// <param name="isRead">Is read notification.</param>
        /// <param name="answers">Answers notification.</param>
        /// <param name="types">Types notification.</param>
        /// <param name="sortByDirection">Sort by direction.</param>
        /// <param name="pageIndex">Page index.</param>
        /// <param name="pageSize">Page size.</param>
        /// <returns>The task paginated list of <see cref="Notification"/>.</returns>
        public Task<PaginatedList<Notification>> GetNotificationsAsync(
            string search,
            bool? isRead,
            string answers,
            string sortByDirection,
            int pageIndex,
            int pageSize,
            IEnumerable<string> types = default,
            IEnumerable<string> fromUserIds = default,
            IEnumerable<string> toUserIds = default)
        {
            return notificationsRepository.GetPaginatedAsync(
                fromUserIds: fromUserIds,
                toUserIds: toUserIds,
                search: search,
                isRead: isRead,
                answers: answers,
                types: types,
                sortByDirection: sortByDirection,
                pageIndex: pageIndex,
                pageSize: pageSize);
        }

        /// <summary>
        /// Get notification by id.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <returns>The task.</returns>
        public Task<Notification> GetNotificationAsync(
            string notificationId)
        {
            return notificationsRepository.GetByIdAsync(notificationId);
        }

        /// <summary>
        /// Get notification by id for user.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userId">User id.</param>
        /// <returns>The task.</returns>
        public Task<Notification> GetNotificationAsync(
            string userId,
            string notificationId)
        {
            return notificationsRepository.GetAsync(userId, notificationId);
        }

        /// <summary>
        /// Get notification by id for users.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userIds">Collection of user id.</param>
        /// <returns>The task.</returns>
        public Task<Notification> GetNotificationAsync(
            IEnumerable<string> userIds,
            string notificationId)
        {
            return notificationsRepository.GetAsync(userIds, notificationId);
        }

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public async Task<Notification> AddAsync(
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments)
        {
            return await AddWithIdAsync(
                notificationId: default,
                userFrom,
                userTo,
                type,
                subject,
                body,
                answerId,
                attachments);
        }

        /// <summary>
        /// Add notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userTo">To user.</param>
        /// <param name="type">Notification type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public async Task<Notification> AddWithIdAsync(
            string notificationId,
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments)
        {
            var notification = CreateNotification(
                notificationId,
                userFrom,
                userTo,
                type,
                answerId,
                subject,
                body,
                attachments);
            await notificationsRepository.AddAsync(notification);

            await SendAsync(notification);
            return notification;
        }

        /// <summary>
        /// Add notification with id by rule.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public async Task<Notification> AddWithIdByRuleAsync(
            string notificationId,
            NotificationUser userFrom,
            string ruleId,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments)
        {
            var usersTo = await rulesRepository.GetUsersForRuleAsync(ruleId);
            if (usersTo?.Count < 1)
            {
                throw new NotificationUsersForRuleNotFoundException(ruleId);
            }

            var notification = CreateNotification(
                notificationId,
                userFrom,
                usersTo.FirstOrDefault().User,
                ruleId,
                answerId,
                subject,
                body,
                attachments);
            await notificationsRepository.AddAsync(notification);

            await SendAsync(notification);

            return notification;
        }

        /// <summary>
        /// Add notifications by rule.
        /// </summary>
        /// <param name="userFrom">From user.</param>
        /// <param name="ruleId">Rule id and type.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <returns>The task.</returns>
        public async Task<ICollection<Notification>> AddByRuleAsync(
            NotificationUser userFrom,
            string ruleId,
            string subject,
            string body,
            char answerId,
            IEnumerable<string> attachments)
        {
            var usersTo = await rulesRepository.GetUsersForRuleAsync(ruleId);
            if (usersTo?.Count < 1)
            {
                throw new NotificationUsersForRuleNotFoundException(ruleId);
            }

            var notifications = usersTo.Select(userTo
                => CreateNotification(
                    default,
                    userFrom,
                    userTo.User,
                    ruleId,
                    answerId,
                    subject,
                    body,
                    attachments)).ToList();
            await notificationsRepository.AddAsync(notifications);

            await Task.WhenAll(notifications.Select(async notification => await SendAsync(notification)).ToArray());

            return notifications;
        }

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <param name="notificationId">Notification id.</param>
        /// <param name="userFrom">From user.</param>
        /// <param name="userForwardTo">To forward user.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <param name="type">Notification type.</param>
        /// <returns>The task.</returns>
        public async Task<Notification> AddAnswerAsync(
            string notificationId,
            char answerId,
            NotificationUser userFrom,
            string subject,
            string body,
            IEnumerable<string> attachments,
            NotificationUser userForwardTo = default,
            string type = default)
        {
            var notification = await notificationsRepository.GetAsync(userFrom.UserId, notificationId)
                ?? throw new NotificationNotFoundException(notificationId);

            return await AddAnswerAsync(
                notification,
                answerId,
                subject,
                body,
                attachments,
                userForwardTo,
                type);
        }

        /// <summary>
        /// Add answer notification.
        /// </summary>
        /// <param name="notification">Notification.</param>
        /// <param name="userForwardTo">To forward user.</param>
        /// <param name="subject">Notification subject.</param>
        /// <param name="answerId">Notification answer id.</param>
        /// <param name="body">Notification body.</param>
        /// <param name="attachments">Array of id attachments.</param>
        /// <param name="type">Notification type.</param>
        /// <returns>The task.</returns>
        public async Task<Notification> AddAnswerAsync(
            Notification notification,
            char answerId,
            string subject,
            string body,
            IEnumerable<string> attachments,
            NotificationUser userForwardTo = default,
            string type = default)
        {
            if (!(notification.NotificationAnswerId == NotificationAnswerConstants.NoAnswer
                || notification.NotificationAnswerId == NotificationAnswerConstants.Pending))
            {
                throw new NotificationRepeatedAnswerException(
                    notification.NotificationId,
                    NotificationAnswerIdExtensions.AnswerIdToString(notification.NotificationAnswerId),
                    NotificationAnswerIdExtensions.AnswerIdToString(answerId));
            }
            var notificationAnswer = CreateNotification(
                notificationId: default,
                userFrom: notification.ToUser,
                userTo: userForwardTo ?? notification.FromUser,
                type: type ?? notification.Type,
                answerId: answerId,
                subject: subject,
                body: body,
                attachments: attachments);
            notificationAnswer.PreviousAnswerId = answerId;
            notificationAnswer.PreviousNotificationId = notification.NotificationId;

            await notificationsRepository.AddAsync(notificationAnswer);

            await SendAsync(notificationAnswer);
            return notificationAnswer;
        }

        private Notification CreateNotification(
            string notificationId,
            NotificationUser userFrom,
            NotificationUser userTo,
            string type,
            char? answerId,
            string subject,
            string body,
            IEnumerable<string> attachments)
        {
            return new Notification
            {
                Body = body,
                Type = type,
                IsRead = false,
                ToUser = userTo,
                Subject = subject,
                FromUser = userFrom,
                SentAt = DateTime.UtcNow,
                NotificationAnswerId = answerId ?? NotificationAnswerConstants.NoAnswer,
                NotificationId = notificationId ?? IdentifierExtensions.GenerateIdentifier(options.IdLength),
                Attachments = attachments?.Select(attachmentId => new NotificationAttachment { NotificationAttachmentId = attachmentId })?.ToList()
            };
        }

        /// <summary>
        /// Add file.
        /// </summary>
        /// <param name="notificationFile">Notification file.</param>
        public Task<NotificationFile> AddFileAsync(
            NotificationFile notificationFile)
        {
            return notificationsRepository.AddFileAsync(notificationFile);
        }

        /// <summary>
        /// Get file by id.
        /// </summary>
        /// <param name="notificationAttachmentId">Notification attachment id.</param>
        public Task<NotificationFile> GetFileAsync(
            string notificationAttachmentId)
        {
            return notificationsRepository.GetFileAsync(notificationAttachmentId);
        }

        /// <summary>
        /// Add attachment.
        /// </summary>
        /// <param name="notificationAttachment">Notification attachment.</param>
        public Task<NotificationAttachment> AddAttachmentAsync(
            NotificationAttachment notificationAttachment)
        {
            return notificationsRepository.AddAttachmentAsync(notificationAttachment);
        }

        /// <summary>
        /// Add attachments.
        /// </summary>
        /// <param name="notificationAttachments">Notification attachments.</param>
        public Task<IEnumerable<NotificationAttachment>> AddAttachmentsAsync(
            IEnumerable<NotificationAttachment> notificationAttachments)
        {
            return notificationsRepository.AddAttachmentsAsync(notificationAttachments);
        }
    }
}