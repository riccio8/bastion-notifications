﻿namespace Bastion.Notifications.Options
{
    /// <summary>
    /// Represents all the options you can use to configure the notifications system.
    /// </summary>
    public class NotificationsOptions
    {
        /// <summary>
        /// Gets or sets the Id length for the notifications system.
        /// </summary>
        public int IdLength { get; set; } = 8;
    }
}